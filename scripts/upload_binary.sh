#!/usr/bin/env bash

function _main {
    retry_count=10
    until [ ${_done} ]; do
        disk_found=$(ls "/dev/disk/by-id/"|grep "RPI")
        if [ -n "${disk_found}" ]; then
            sleep 2
            upload_uf2 $1 $2
            _done=1
        else
            if [ $retry_count -eq 0 ]; then
                _done=1
                echo "RPi not found"
            else
                retry_count=$((retry_count-1))
                echo -n ". "
                sleep 1
            fi
        fi
    done
}

function upload_uf2 {
    echo ${2}
    echo "start" &&
    echo "pwd: $(pwd)" &&
    echo "mount /mnt/rp" &&
    mount /mnt/rp &&
    echo "cp ${1}/${2}.uf2" &&
    cp ${1}/${2}.uf2 /mnt/rp &&
    echo "umount" &&
    umount /mnt/rp &&
    echo "done!"
}

if [[ ${BASH_SOURCE[0]} == "${0}" ]]; then
    _main $1 $2
fi
