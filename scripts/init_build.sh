#!/usr/bin/env bash
build_type=debug
echo "rm ./${build_type}"
sleep 1; echo -n ". "
sleep 1; echo -n ". "
sleep 1; echo "."
sleep 1;

rm -r ./${build_type}
echo "mkdir" &&
mkdir ./${build_type} &&
echo "cd ${build_type}" &&
cd ./${build_type} &&
cmake -DCMAKE_BUILD_TYPE=${build_type} .. &&
echo "$build_type done!\n\n"

cd ..

build_type=test
rm -r ./${build_type}
echo "mkdir" &&
mkdir ./${build_type} &&
echo "cd ${build_type}" &&
cd ./${build_type} &&
cmake -DCMAKE_BUILD_TYPE=${build_type} .. &&
echo "$build_type done!"
