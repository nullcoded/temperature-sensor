vim.keymap.set("i", "<F2>", "<esc>:CMakeBuild -j32<CR>", { desc = "build" })
vim.keymap.set("i", "<F3>", "<esc>:CMakeInstall<CR>", { desc = "install" })
vim.keymap.set("i", "<F4>", "<esc>:CMakeTest --rerun-failed --output-on-failure<CR>", { desc = "test" })

vim.keymap.set("n", "<F2>", ":CMakeBuild -j32<CR>", { desc = "build" })
vim.keymap.set("n", "<F3>", ":CMakeInstall<CR>", { desc = "install" })
vim.keymap.set("n", "<F4>", ":CMakeTest --rerun-failed --output-on-failure", { desc = "test" })
