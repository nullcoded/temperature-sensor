/**
 *	UnitTest Flash  - test read, write and clear
 */

#include "memory/flash.h"

#include "ncl/buffers/ArraySlice.h"
#include "ncl/logging/logging.h"
#include "ncl/util/misc.h"
#include "ncl/util/types.h"
#include "pico/platform.h"
#include "pico/stdlib.h"

bool test_erase_all()
{
    memory::Flash flash{};
    if (flash.init().is_error()) {
        return false;
    }
    if (flash.blocking_erase_full().is_error()) {
        LOG_E("blocking_erase_full failed");
        return false;
    }
    const std::size_t buf_len = 32;
    std::uint8_t buf[buf_len];
    ssize_t read_count = 0;
    if (flash.read_data(buf, buf_len, 0x00, read_count).is_error()) {
        LOG_E("read_data failed");
        return false;
    }
    const std::size_t expected_value = 0xff;
    bool pass = true;
    for (std::size_t count = 0; count < buf_len; ++count) {
        if (buf[count] != expected_value) {
            LOG_E("expected_value: %02x, found: %02x", expected_value, buf[count]);
            pass = false;
            break;
        }
    }
    return pass;
}

bool test_write_read_batch()
{
    LOG_E("func: test_write_read_byte");
    std::uint8_t test_values[]{ 0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9,  0xa,
                                0xb,  0xc,  0xd,  0xe,  0xf,  0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
                                0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f };
    std::size_t test_values_len = sizeof(test_values);
    std::size_t write_count = 0;
    const memory::addr_t address = 1;

    memory::Flash flash{};
    if (flash.init().is_error()) {
        return false;
    }

    if (flash.set_status(1, 0, 0, 0, 0, 0).is_error()) {
        return false;
    }
    if (flash.blocking_write(address, test_values, test_values_len, write_count).is_error()) {
        LOG_E("blocking_write failed");
        return false;
    }
    std::uint8_t read_values[test_values_len];
    ssize_t read_count = 0;
    if (flash.read_data(read_values, test_values_len, address, read_count).is_error()) {
        LOG_E("read_data failed");
        return false;
    }

    LOG("test_values:");
    util::print_array(test_values, test_values_len);
    util::print_array(read_values, test_values_len);
    return util::compare_array(test_values, test_values_len, read_values, test_values_len);
}

bool test_write_read_byte()
{
    LOG("func: test_write_read_byte");
    const std::uint8_t test_value = 42;
    memory::Flash flash{};
    if (flash.init().is_error()) {
        return false;
    }

    if (flash.set_status(1, 0, 0, 0, 0, 0).is_error()) {
        return false;
    }
    if (flash.blocking_set_byte(0, test_value).is_error()) {
        LOG_E("blocking_set_byte failed");
        return false;
    }
    memory::addr_t address = 0;
    std::uint8_t read_value = 42;
    ssize_t read_count = 0;
    if (flash.read_data(&read_value, 1, address, read_count).is_error()) {
        LOG_E("read_data failed");
        return false;
    }

    LOG("test_value: %u, read_value: %u", test_value, read_value);
    return test_value == read_value;
}

bool test_jedec()
{
    LOG("func: test_jedec");
    memory::Flash flash{};
    const std::size_t jedec_data_size = 3;
    std::uint8_t jedec_expected_data[jedec_data_size] = { 0xbf, 0x25, 0x8e };
    std::uint8_t data[jedec_data_size];
    ssize_t read_count = 0;
    if (flash.init().is_error()) {
        return false;
    }
    if (flash.read_jedec(data, jedec_data_size, read_count).is_error()) {
        return false;
    }
    bool pass = util::compare_array(jedec_expected_data, jedec_data_size, data, jedec_data_size);
    LOG("expected data:");
    util::print_array(jedec_expected_data, jedec_data_size);
    LOG("data:");
    util::print_array(data, jedec_data_size);
    return pass;
}

bool flash_test_raw_jedec()
{
    using pin_t = uint;
    const struct SpiPins {
        pin_t cs;             // chip select
        pin_t sck;            // clock
        pin_t mosi;           // tx
        pin_t miso;           // rx
    } pins = { .cs = 17,      // chip select
               .sck = 18,     // clock
               .mosi = 19,    // tx
               .miso = 16 };  // struct SpiPins
    const struct SpiFormat {
        uint bits_count;            // number of bits per transfer [4..16]
        spi_cpol_t clock_polarity;  // SPI_CPOL_0 = clock kept low when not in use, ___-_-
                                    // SPI_CPOL_1 = clock kept high when not in use ---_-_
        spi_cpha_t clock_phase;     // SPI_CPHA_0 = capture on first clock transition, ---|_-_
                                    // SPI_CPHA_1 = capture on second clock transition ---_|-_
        spi_order_t bits_order;     // SPI_LSP_FIRST, SPI_MSB_FIRST
    } format = { .bits_count = 8,
                 .clock_polarity = SPI_CPOL_1,  // set polarity and phase to capture
                 .clock_phase = SPI_CPHA_1,     // on the rising edge
                 .bits_order = SPI_MSB_FIRST };
    const uint baudrate = 25 * 1000000;
    bool pass = false;
    std::uint8_t dummy_value = 0;

    gpio_init(pins.cs);
    gpio_set_dir(pins.cs, GPIO_OUT);
    // cs disable
    // asm volatile("nop \n nop \n nop");
    gpio_put(pins.cs, true);
    // asm volatile("nop \n nop \n nop");
    //  setup spi
    spi_init(spi0, baudrate);
    spi_set_format(
      spi0, format.bits_count, format.clock_polarity, format.clock_phase, format.bits_order);
    // setup spi pins
    gpio_set_function(pins.sck, GPIO_FUNC_SPI);
    gpio_set_function(pins.mosi, GPIO_FUNC_SPI);
    gpio_set_function(pins.miso, GPIO_FUNC_SPI);

    busy_wait_us(50);
    // read jedec
    const std::size_t buf_len = 3;
    std::uint8_t cmd = 0x9f;
    std::uint8_t buf[buf_len] = {};
    // cs enable
    // asm volatile("nop \n nop \n nop");
    gpio_put(pins.cs, false);
    // asm volatile("nop \n nop \n nop");
    std::size_t bytes_written = spi_write_blocking(spi0, &cmd, 1);
    std::size_t read_len = spi_read_blocking(spi0, dummy_value, buf, buf_len);
    // cs disable
    // asm volatile("nop \n nop \n nop");
    gpio_put(pins.cs, true);
    // asm volatile("nop \n nop \n nop");
    LOG_D("written: %u, read: %u", bytes_written, read_len);
    LOG_D("data: %02x %02x %02x", buf[0], buf[1], buf[2]);
    spi_deinit(spi0);
    gpio_deinit(pins.cs);
    if (buf[0] == 0xbf && buf[1] == 0x25 && buf[2] == 0x8e) {
        pass = true;
    }
    return pass;
}

bool flash_test_raw_read()
{
    using pin_t = uint;
    const struct SpiPins {
        pin_t cs;             // chip select
        pin_t sck;            // clock
        pin_t mosi;           // tx
        pin_t miso;           // rx
    } pins = { .cs = 17,      // chip select
               .sck = 18,     // clock
               .mosi = 19,    // tx
               .miso = 16 };  // struct SpiPins
    const struct SpiFormat {
        uint bits_count;            // number of bits per transfer [4..16]
        spi_cpol_t clock_polarity;  // SPI_CPOL_0 = clock kept low when not in use, ___-_-
                                    // SPI_CPOL_1 = clock kept high when not in use ---_-_
        spi_cpha_t clock_phase;     // SPI_CPHA_0 = capture on first clock transition, ---|_-_
                                    // SPI_CPHA_1 = capture on second clock transition ---_|-_
        spi_order_t bits_order;     // SPI_LSP_FIRST, SPI_MSB_FIRST
    } format = { .bits_count = 8,
                 .clock_polarity = SPI_CPOL_1,  // set polarity and phase to capture
                 .clock_phase = SPI_CPHA_1,     // on the rising edge
                 .bits_order = SPI_MSB_FIRST };
    const uint baudrate = 25 * 1000000;
    bool pass = false;
    std::uint8_t dummy_value = 0;

    gpio_init(pins.cs);
    gpio_set_dir(pins.cs, GPIO_OUT);
    // cs disable
    // asm volatile("nop \n nop \n nop");
    gpio_put(pins.cs, true);
    // asm volatile("nop \n nop \n nop");
    //  setup spi
    spi_init(spi0, baudrate);
    spi_set_format(
      spi0, format.bits_count, format.clock_polarity, format.clock_phase, format.bits_order);
    // setup spi pins
    gpio_set_function(pins.sck, GPIO_FUNC_SPI);
    gpio_set_function(pins.mosi, GPIO_FUNC_SPI);
    gpio_set_function(pins.miso, GPIO_FUNC_SPI);
    busy_wait_us(50);
    // read jedec
    const std::size_t buf_len = 42;
    std::uint8_t cmd_buf[] = { 0x03, 0x00, 0x00, 0x00 };
    std::uint8_t buf[buf_len] = {};
    // cs disable
    // asm volatile("nop \n nop \n nop");
    gpio_put(pins.cs, false);
    // asm volatile("nop \n nop \n nop");
    //  std::size_t bytes_written =
    spi_write_blocking(spi0, cmd_buf, sizeof(cmd_buf));
    // std::size_t read_len =
    spi_read_blocking(spi0, dummy_value, buf, buf_len);
    // cs disable
    // asm volatile("nop \n nop \n nop");
    gpio_put(pins.cs, true);
    // asm volatile("nop \n nop \n nop");
    //  LOG_D("written: %u, read: %u", bytes_written, read_len);
    //  LOG_D("data:");
    util::print_array(buf, buf_len);
    spi_deinit(spi0);
    gpio_deinit(pins.cs);
    pass = true;
    for (std::size_t i = 0; i < buf_len; ++i) {
        if (buf[i] != 0xff) {
            LOG_E("read_data[%u]=0x%02x", i, buf[i]);
            pass = false;
            break;
        }
    }
    return pass;
}

bool test_write_storagehandler()
{
    bool status = false;
    std::uint8_t test_data[]
      = { 0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9,  0x1,  0xb,  0xc,  0xd,  0xe,
          0xf,  0x10, 0x11, 0x12, 0x13, 0x2,  0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
          0x3,  0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x4,  0x29, 0x2a, 0x2b, 0x2c,
          0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x5,  0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b,
          0x6,  0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x7,  0x47, 0x48, 0x49, 0x4a,
          0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x8,  0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
          0x9,  0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0xa,  0x65, 0x66, 0x67, 0x68,
          0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0xb,  0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
          0xc,  0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f };
    const std::size_t test_data_size = sizeof(test_data);
    buffers::ArrayBuffer buf_data{ test_data_size, &(test_data[0]) };

    memory::FlashStorageHandler storage{};

    if (storage.init().is_error()) {
        return false;
    }

    if (storage.write(buf_data).is_error()) {
        storage.close();
        return false;
    }

    memory::Flash &flash = *storage.get_io();

    std::uint8_t read_data[test_data_size] = {};
    ssize_t read_count = 0;
    if (flash.read_data(read_data, test_data_size, 0, read_count).is_error()) {
        LOG_E("error:%s", "read_data");
    }
    storage.close();

    status = util::compare_array(read_data, test_data_size, test_data, test_data_size);
    if (!status) {
        util::print_array(read_data, test_data_size);
    }
    return status;
}
bool test_read_storagehandler()
{
    bool status = false;
    std::uint8_t test_data[]
      = { 0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9,  0x1,  0xb,  0xc,  0xd,  0xe,
          0xf,  0x10, 0x11, 0x12, 0x13, 0x2,  0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
          0x3,  0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x4,  0x29, 0x2a, 0x2b, 0x2c,
          0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x5,  0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b,
          0x6,  0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x7,  0x47, 0x48, 0x49, 0x4a,
          0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x8,  0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
          0x9,  0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0xa,  0x65, 0x66, 0x67, 0x68,
          0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0xb,  0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
          0xc,  0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f };
    const std::size_t test_data_size = sizeof(test_data);
    buffers::ArrayBuffer buf_data{ test_data_size };
    memory::FlashStorageHandler storage{};

    if (storage.init().is_error()) {
        return false;
    }

    if (storage.read(buf_data).is_error()) {
        storage.close();
    }

    std::uint8_t *read_data = buf_data.data();

    status = util::compare_array(read_data, test_data_size, test_data, test_data_size);
    if (!status) {
        util::print_array(read_data, test_data_size);
    }

    return status;
}

bool test_clear_storagehandler()
{
    bool status = false;
    const std::size_t test_data_size = 128;
    buffers::ArrayBuffer buf_data{ test_data_size };
    memory::FlashStorageHandler storage{};

    if (storage.init().is_error()) {
        return false;
    }

    if (storage.clear().is_error()) {
        storage.close();
    }

    memory::Flash &flash = *storage.get_io();

    std::uint8_t read_data[test_data_size] = {};
    ssize_t read_count = 0;
    if (flash.read_data(read_data, test_data_size, 0, read_count).is_error()) {
        LOG_E("error:read_data");
    }
    storage.close();
    status = true;
    for (std::size_t i = 0; i < test_data_size; ++i) {
        if (read_data[i] != 0xff) {
            LOG("read_data[%u]=0x%02x", i, read_data[i]);
            status = false;
            break;
        }
    }
    if (!status) {
        util::print_array(read_data, test_data_size);
    }
    return status;
}

bool test_keystore_write()
{
    bool status = false;
    util::keystore::Crc32Hash hasher{};
    memory::FlashStorageHandler storage{};
    const size_t storage_size = (1 * util::mb) / 8;
    util::keystore::Keystore<storage_size> ks{ util::keystore::hash_fn_t(hasher), &storage };
    const char key1[] = "one";
    uint32_t val1 = 1;
    if (ks.set(key1,
               strlen(key1),
               reinterpret_cast<uint8_t *>(&val1),
               sizeof(val1),
               util::types::arg_t::u32)
          .is_error()) {
        LOG_E("failed to set %s:%d", key1, val1);
        return false;
    }
    const char key2[] = "two";
    float val2 = 2.0;
    if (ks.set(key2,
               strlen(key2),
               reinterpret_cast<uint8_t *>(&val2),
               sizeof(val2),
               util::types::arg_t::f32)
          .is_error()) {
        LOG_E("failed to set %s:%d", key2, val2);
        return false;
    }
    if (ks.serialize().is_error()) {
        LOG_E("failed to serialize");
        return false;
    }

    memory::Flash &flash = *storage.get_io();
    flash.init();

    const std::size_t read_data_size = 128;
    std::uint8_t meta_data[] =
      // meta
      { 0xa5, 0xf3, 0x75, 0x24, 0x4b, 0x8b, 0x64, 0x05, 0x00, 0x00, 0x00, 0x01, 0x02, 0x02, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    std::uint8_t key_data[] =
      // key 1
      { 0x66, 0x8a, 0xca, 0x11, 0x80, 0x01, 0x00, 0x00, 0x03, 0x74, 0x77, 0x6f, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
      // key 2
        0xf1, 0x86, 0x6c, 0x7a, 0x00, 0x01, 0x00, 0x00, 0x03, 0x6f, 0x6e, 0x65, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00 };
    std::uint8_t value1_data[] =
      // value 1
      { 0x03, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    std::uint8_t value2_data[] =
      // value 2
      { 0x09, 0x04, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    std::uint8_t read_data[read_data_size] = {};
    ssize_t read_count = 0;
    if (flash.read_data(read_data, read_data_size, 0, read_count).is_error()) {
        LOG_E("compare meta_data error:read_data");
    }
    status = util::compare_array(meta_data, read_data_size, read_data, read_data_size);
    if (false == status) {
        util::print_array(read_data, read_data_size);
    }

    if (status) {
        if (flash.read_data(read_data, read_data_size, 128, read_count).is_error()) {
            LOG_E("compare key_data error:read_data");
        }
        status = util::compare_array(key_data, read_data_size, read_data, read_data_size);
        if (false == status) {
            util::print_array(read_data, read_data_size);
        }
    }

    if (status) {
        if (flash.read_data(read_data, read_data_size, 256, read_count).is_error()) {
            LOG_E("compare value1_data error:read_data");
        }
        status = util::compare_array(value1_data, read_data_size, read_data, read_data_size);
        if (false == status) {
            util::print_array(read_data, read_data_size);
        }
    }

    if (status) {
        if (flash.read_data(read_data, read_data_size, 384, read_count).is_error()) {
            LOG_E("compare value2_data error:read_data");
        }
        status = util::compare_array(value2_data, read_data_size, read_data, read_data_size);
        if (false == status) {
            util::print_array(read_data, read_data_size);
        }
    }
    flash.deinit();
    return status;
}

bool test_keystore_read()
{
    bool status = true;
    const size_t storage_size = (1 * util::mb) / 8;
    util::keystore::Crc32Hash hasher{};
    memory::FlashStorageHandler storage{};
    util::keystore::Keystore<storage_size> ks{ util::keystore::hash_fn_t(hasher), &storage };
    ks.deserialize();
    util::Crc32 crc_gen{};
    //util::keystore::hash_t hash1_b = crc_gen.calc(reinterpret_cast<const uint8_t *>(key1), strlen(key1));
    uint32_t val1_b = 0;
    std::size_t val1_len = sizeof(val1_b);
    const char key1[] = "one";
    uint32_t val1 = 1;
    ks.get(
      key1, strlen(key1), reinterpret_cast<uint8_t *>(&val1_b), val1_len, util::types::arg_t::u32);
    if (val1 != val1_b) {
        LOG_E("failed to get val1: %lu != %lu", val1, val1_b);
        return false;
    }
    const char key2[] = "two";
    util::keystore::hash_t hash_key2
      = crc_gen.calc(reinterpret_cast<const uint8_t *>(key2), strlen(key2));
    float val2_b = 0;
    std::size_t val2_len = sizeof(val2_b);
    ks.get(hash_key2, reinterpret_cast<uint8_t *>(&val2_b), val2_len, util::types::arg_t::f32);
    float val2 = 2.0;
    if (val2 != val2_b) {
        LOG_E("failed to get val2: %f != %f", val2, val2_b);
        util::print_array(reinterpret_cast<uint8_t *>(&val2), sizeof(val2));
        util::print_array(reinterpret_cast<uint8_t *>(&val2_b), sizeof(val2_b));
        return false;
    }
    return status;
}

bool test_keystore_clear()
{
    bool status = false;
    const size_t storage_size = (1 * util::mb) / 8;
    util::keystore::Crc32Hash hasher{};
    memory::FlashStorageHandler storage{};
    util::keystore::Keystore<storage_size> ks{ util::keystore::hash_fn_t(hasher), &storage };
    status = ks.clear().is_okay();
    if(status)
    {
        const std::size_t buf_len = 128;
        std::uint8_t buf[buf_len];
        ssize_t read_count = 0;
        memory::Flash &flash = *storage.get_io();
        flash.init();
        if (flash.read_data(buf, buf_len, 0x00, read_count).is_error()) {
            LOG_E("read_data failed");
            return false;
        }
        flash.deinit();
        const std::size_t expected_value = 0xff;
        for (std::size_t count = 0; count < buf_len; ++count) {
            if (buf[count] != expected_value) {
                LOG_E("expected_value: %02x, found: %02x", expected_value, buf[count]);
                status = false;
                break;
            }
        }
        if(!status)
        {
            util::print_array(buf, buf_len);
        }
    }
    return status;
}

int init_test()
{
    LOG("\n\n\n\ninit_test begin!\n");
    std::pair<std::string, util::test_fn> tests[] = {
        //{ "flash_test_raw_jedec", flash_test_raw_jedec },
        //{ "flash_test_raw_read", flash_test_raw_read },
        //{ "test_jedec", test_jedec },
        //{ "test_write_read_byte", test_write_read_byte },
        //{ "test_write_read_batch", test_write_read_batch },
        { "test_erase_all", test_erase_all },
        //{ "test_write_storagehandler", test_write_storagehandler },
        //{ "test_read_storagehandler", test_read_storagehandler },
        //{ "test_clear_storagehandler", test_clear_storagehandler },
        { "test_keystore_write", test_keystore_write },
        { "test_keystore_read", test_keystore_read },
        { "test_keystore_clear", test_keystore_clear },
    };
    bool all_success = true;
    uint pass_test_count = 0;
    uint test_count = 0;
    for (std::pair<std::string, util::test_fn> pair : tests) {
        LOG("\n**>\ttest:%s::start\n", pair.first.c_str());
        bool test_success = pair.second();
        test_count++;
        if (test_success) {
            pass_test_count++;
        }
        LOG("**\ttest:%s::%s\n", pair.first.c_str(), test_success ? "PASS" : "FAIL");
        all_success = all_success && test_success;
    }
    LOG("****\ttest cases %s, %u/%u\n", all_success ? "PASS" : "FAIL", pass_test_count, test_count);
    return all_success ? 0 : -1;
}

int main()
{
    stdio_init_all();
    util::init(&time_us_64);
    logging::init(logging::log_level_t::verbose);
    int stat = init_test();
    LOG("DONE!\n\n\n");
    return stat;
}
