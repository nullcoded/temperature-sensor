/**
 *	UnitTest Timmer  - test read, write and clear
 */

#include <cstdint>
#include "timmed/TimmedTrigger.h"
#include "io/TriggerCtlW.h"
#include "io/TriggerCtl.h"

#include "ncl/buffers/ArraySlice.h"
#include "ncl/logging/logging.h"
#include "ncl/util/misc.h"
#include "ncl/util/types.h"
#include "pico/platform.h"
#include "pico/stdlib.h"

bool test_on_off()
{
    bool pass = true;
    const io::pin_t pin_trigger = 22; //relay trigger
    const io::pin_t pin_test = 21; //test read
    gpio_init(pin_test);
    gpio_set_dir(pin_test, GPIO_IN);
    io::TriggerCtlGpio trigger{pin_trigger};
    trigger.init();
    trigger.set_on();
    sleep_ms(util::ms_per_s*2);
    pass = gpio_get(pin_test);
    trigger.set_off();
    sleep_ms(util::ms_per_s*2);
    pass = !gpio_get(pin_test);
    return pass;
}

bool test_on_off_wled()
{
    //need a light sensor to test if led is on
    bool pass = true;
    const io::pin_t pin_trigger = 0x00; //led trigger
    io::TriggerCtlW trigger{pin_trigger};
    trigger.init();
    trigger.set_on();
    sleep_ms(util::ms_per_s*2);
    trigger.set_off();
    sleep_ms(util::ms_per_s*2);
    return pass;
}

bool test_timer_trigger()
{
    bool pass = true;
    const io::pin_t pin_trigger = 22; //relay trigger
    const io::pin_t pin_test = 21; //test read
    const std::uint32_t duration = 5;
    gpio_init(pin_test);
    gpio_set_dir(pin_test, GPIO_IN);
    io::TriggerCtlGpio trigger{pin_trigger};
    timmed::TimmedTrigger timmer{trigger, duration};
    std::uint64_t now = 1725131521; // 8/31/2024 - 12:12:01 pm - saturday
    std::uint32_t delay = 10;
    std::uint32_t extra = 5;
    timmer.setNextStart(now+delay);
    timmer.begin();
    for(uint seconds = 0; seconds <= delay+duration+extra; ++seconds)
    {
        //LOG_D("time: %llus", seconds);
        timmer.poll(now+seconds);
        if(seconds<delay)
        {
            pass = !gpio_get(pin_test);
            if(!pass)
            {
                LOG_E("error at %llus: should be off during delay", now+seconds);
            }
        } else if (seconds<delay+duration) {
            pass = gpio_get(pin_test);
            if(!pass)
            {
                LOG_E("error at %llus: should be on during duration", now+seconds);
            }
        } else {
            pass = !gpio_get(pin_test);
            if(!pass)
            {
                LOG_E("error at %llus: should be off after duration", now+seconds);
            }
        }
        if(!pass)
        {
            return pass;
        }
        sleep_ms(util::ms_per_s/4);
    }
    return pass;
}

bool test_timer_trigger_destructor()
{
    bool pass = true;
    const io::pin_t pin_trigger = 22; //relay trigger
    const io::pin_t pin_test = 21; //test read
    const std::uint32_t duration = 5;
    gpio_init(pin_test);
    gpio_set_dir(pin_test, GPIO_IN);
    std::uint64_t now = 1725131521; // 8/31/2024 - 12:12:01 pm - saturday
    std::uint32_t delay = 10;
    std::uint32_t extra = 5;
    {
        io::TriggerCtlGpio trigger{pin_trigger};
        timmed::TimmedTrigger timmer{trigger, duration};
        timmer.setNextStart(now+delay);
        timmer.begin();
        for(uint seconds = 0; seconds <= delay+duration+extra; ++seconds)
        {
            //LOG_D("time: %us", seconds);
            timmer.poll(now+seconds);
            if(seconds<delay)
            {
                pass = !gpio_get(pin_test);
                if(!pass)
                {
                    LOG_E("error at %llus: should be off during delay", seconds);
                }
            } else if (seconds<delay+duration) {
                pass = gpio_get(pin_test);
                if(!pass)
                {
                    LOG_E("error at %llus: should be on during duration", seconds);
                }
                break;
            }
            sleep_ms(util::ms_per_s/4);
        }
    }
    pass = !gpio_get(pin_test);
    if(!pass)
    {
        LOG_E("error at destructor: should be off after duration");
    }
    return pass;
}


int init_test()
{
    LOG("\n\n\n\ninit_test begin!\n");
    std::pair<std::string, util::test_fn> tests[] = {
        { "test_on_off", test_on_off },
        //{ "test_on_off_wled", test_on_off_wled },
        { "test_timer_trigger", test_timer_trigger },
        { "test_timer_trigger_destructor", test_timer_trigger_destructor },
    };
    bool all_success = true;
    uint pass_test_count = 0;
    uint test_count = 0;
    for (std::pair<std::string, util::test_fn> pair : tests) {
        LOG("\n**>\ttest:%s::start\n", pair.first.c_str());
        bool test_success = pair.second();
        test_count++;
        if (test_success) {
            pass_test_count++;
        }
        LOG("**\ttest:%s::%s\n", pair.first.c_str(), test_success ? "PASS" : "FAIL");
        all_success = all_success && test_success;
    }
    LOG("****\ttest cases %s, %u/%u\n", all_success ? "PASS" : "FAIL", pass_test_count, test_count);
    return all_success ? 0 : -1;
}

int main()
{
    stdio_init_all();
    util::init(&time_us_64);
    logging::init(logging::log_level_t::verbose);
    net::WifiDriver::init();
    int stat = init_test();
    LOG("DONE!\n\n\n");
    return stat;
}
