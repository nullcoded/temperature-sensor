#ifndef SENACK_PROTOCOL_H
#define SENACK_PROTOCOL_H

#include <string>
#include <vector>

#include "protocol/Protocol.h"

namespace senack {

using seq_t = uint32_t;
using payload_len_t = uint16_t;
using arg_count_t = uint16_t;
enum message_t {
    reserved_mid = 0,
    time_sync_cmd_mid = 1,          // time since epoch ud
    self_identify_mid = 2,          // device UUID, capabilities count, capabilities list
    status_data_mid = 3,            // return a u32 status code
    temperature_f64_data_mid = 4,   // double value, time ts
    humidity_f64_data_mid = 5,      // double value, time ts
    status_mid = 6,                 // double staus, time ts
};

enum capabilities_t {
    temperature_source_cap,
    humidity_source_cap,

};

class Arg {
   public:
    std::string label;
    protocol::arg_t arg_type;
    Arg(std::string l, protocol::arg_t t) : label(l), arg_type(t)
    {
    }
    virtual ~Arg(){
    }
    virtual size_t calc_size()
    {
        return 0;
    }
    virtual util::Status<protocol::status_t> serialize(buffers::Array &buffer)
    {
        util::Status<protocol::status_t> status(protocol::success, protocol::status_labels);
        status = protocol::fail_status;
        return status;
    }
};  // class Args

// single values
template <typename T> class ArgSingle : public Arg {
   public:
    T value;
    ArgSingle(std::string label, protocol::arg_t arg_type, T v) : Arg(label, arg_type), value(v)
    {
    }
    virtual size_t calc_size()
    {
        return sizeof(T);
    }
    virtual util::Status<protocol::status_t> serialize(buffers::Array &buffer)
    {
        util::Status<protocol::status_t> status(protocol::success, protocol::status_labels);
        if (calc_size() > buffer.remaining_space()) {
            status = protocol::buffer_capacity_exceeded_error;
        }
        if (!status.is_error()) {
            buffer.push_type(value);
        }
        return status;
    }
};  // class ArgInt
// array values
template <typename T> class ArgArray : public Arg {
   public:
    arg_count_t count;
    T *p_value;
    // TODO: calc_size, serialize
};  // class ArgArray

/**
 * Header - contains the protocol id that identifies the packet and payload
 * length that includes the footer
 */
class Header {
   public:
    seq_t seq;                  // monotonically increasing sequence number of the buffer
    payload_len_t payload_len;  // how many more bytes to expect

    virtual util::Status<protocol::status_t> serialize(buffers::Array &buffer)
    {
        util::Status<protocol::status_t> status(protocol::success, protocol::status_labels);
        if (header_size > buffer.remaining_space()) {
            status = protocol::buffer_capacity_exceeded_error;
            return status;
        }
        auto buf_status = buffer.push_type(seq);
        if (!buf_status.is_error()) {
            buf_status = buffer.push_type(payload_len);
        }
        if (buf_status.is_error()) {
            status = protocol::buffer_error;
        }
        return status;
    }
    static const size_t header_size = sizeof(seq) + sizeof(payload_len);
    static const size_t payload_len_offset = sizeof(seq);

};  // class Header

/**
 * Message - contains message id, and a set of arguments for the message
 */
class Message : public protocol::Message {
    static const size_t header_offset = sizeof(protocol::message_id_t);
    static const size_t payload_len_offset = header_offset + Header::payload_len_offset;

   public:
    Header header;
    std::vector<Arg *> args;

    ~Message()
    {
        for(Arg *p_arg: args)
        {
            delete p_arg;
        }
        args.clear();

    }

    size_t calc_sizeArgs()
    {
        size_t size = 0;
        for (Arg *arg : args) {
            size += arg->calc_size();
        }
        return size;
    }

    virtual size_t calc_payload_size()
    {
        return calc_sizeArgs();
    }
    virtual util::Status<protocol::status_t> serialize_body(buffers::Array &buffer)
    {
        util::Status<protocol::status_t> status(protocol::success, protocol::status_labels);
        if (calc_payload_size() > buffer.remaining_space()) {
            status = protocol::buffer_capacity_exceeded_error;
            return status;
        }
        header.payload_len = calc_payload_size();
        status = header.serialize(buffer);
        if (!status.is_error()) {
            for (Arg *arg : args) {
                status = arg->serialize(buffer);
                if (status.is_error()) {
                    break;
                }
            }
        }
        if (status.is_error()) {
            LOG_E("Array error:%s", status.as_string().c_str());
        }

        return status;
    }
    static bool isHeaderComplete(buffers::ArrayBuffer &buffer)
    {
        return (header_offset + Header::header_size <= buffer.len());
    }
    static std::optional<payload_len_t> get_payload_len(buffers::ArrayBuffer &buffer)
    {
        if (!isHeaderComplete(buffer)) {
            return {};
        }
        return buffer.net_get_uint_at<payload_len_t>(payload_len_offset).value();
    }
};  // class Message

/**
 * Packet - contains a header, a set of messages and a crc footer
 * the total size of the packet should be size of ProtocolID (4)+ size of seq (4) + size of
 * payload_len (2)+ payload_len
 */
class Packet {
   public:
    protocol::protocol_id_t id;
    std::vector<Message> messages;
    protocol::crc32_t crc32;
};  // class Packet

class Protocol : public protocol::Protocol {
   public:
    static const uint8_t version_major = 0;
    static const uint8_t version_minor = 0;
    static const uint8_t version_patch = 0;
    static const uint8_t version_build = 1;
    // echo -n "senack"|python ../tools/crc32.py
    static const protocol::protocol_id_t protocol_id = 0x6bfa8762;

    struct Members {
        seq_t next_seq;
    } m;

    Protocol()
        : protocol::Protocol(protocol_id,
                             version_major,
                             version_minor,
                             version_patch,
                             version_build),
          m(Members{ .next_seq = 1 })
    {
    }
    virtual bool is_message_complete(buffers::ArrayBuffer &message)
    {
        if (!Message::isHeaderComplete(message)) {
            return false;
        }
        bool has_message_complete = false;
        auto payload_len = Message::get_payload_len(message);
        if (payload_len) {
            has_message_complete = (message.len() >= payload_len.value());
        }
        return has_message_complete;
    }
    virtual std::optional<size_t> get_payload_len(buffers::ArrayBuffer &message)
    {
        return !is_message_complete(message) ? std::nullopt : Message::get_payload_len(message);
    }
    virtual util::Status<protocol::status_t> handle_message(buffers::ArrayBuffer &message)
    {
        util::Status<protocol::status_t> ret_state{ protocol::status_t::success,
                                                    protocol::status_labels };
        if (!is_protocol_message(message)) {
            ret_state = protocol::status_t::protocol_id_mismatch_error;
            return ret_state;
        }

        if (!is_message_complete(message)) {
            ret_state = protocol::status_t::message_incomplete_okay;
            return ret_state;
        }
        // TODO: send to worker thread
        return ret_state;
    }

    seq_t get_seq()
    {
        seq_t seq = m.next_seq;
        m.next_seq += 1;
        return seq;
    }

    util::Status<protocol::status_t> create_data_temperature_double(Message *p_message)
    {
        util::Status<protocol::status_t> status{ protocol::status_t::success,
                                                 protocol::status_labels };

        if (p_message) {
            p_message->m.message_id = temperature_f64_data_mid;
            p_message->args.push_back(
              new ArgSingle<double>{ "temp_value", protocol::arg_t::f64, 0.0 });
            p_message->args.push_back(
              new ArgSingle<util::types::time_us_t>{ "temp_ts", protocol::arg_t::u64, 0 });
            p_message->header.payload_len = p_message->calc_payload_size();
        } else {
            status = protocol::null_pointer_error;
        }

        return status;
    }

    util::Status<protocol::status_t> update_data_temperature_double(double value,
                                                                 util::types::time_us_t ts,
                                                                 Message *p_message)
    {
        util::Status<protocol::status_t> status{ protocol::status_t::success,
                                                 protocol::status_labels };
        if (p_message) {
            p_message->header.seq = get_seq();
            reinterpret_cast<ArgSingle<double> *>(p_message->args[0])->value = value;
            reinterpret_cast<ArgSingle<util::types::time_us_t> *>(p_message->args[1])->value = ts;
        } else {
            status = protocol::null_pointer_error;
        }

        return status;
    }

    static bool is_protocol_message(buffers::ArrayBuffer &buffer)
    {
        auto opt = buffer.net_get_uint_at<uint32_t>(0);
        if (!opt.has_value()) {
            return false;
        }
        uint32_t id = opt.value();
        return protocol_id == id;
    }

};  // class Protocol
}  // namespace senack

#endif  // SENACK_PROTOCOL_H
