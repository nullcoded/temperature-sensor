#ifndef SENACK_PARSER_H
#define SENACK_PARSER_H

#include <map>
#include <string>

#include "ncl/buffers/ArraySlice.h"
#include "ncl/util/State.h"
#include "ncl/util/Status.h"
#include "ncl/util/util.h"
#include "senack/Protocol.h"

namespace senack {

class Parser {
    struct Members {
    } m;
    std::unique_ptr<senack::Protocol> p_protocol;
    std::map<senack::message_t, senack::Message *> messages;

   public:
    Parser() : m(Members{}), p_protocol(nullptr), messages()
    {
    }

    ~Parser()
    {
        for(auto const& [key, val] : messages)
        {
            delete val;
        }
        messages.clear();
    }

    util::Status<protocol::status_t> poll()
    {
        util::Status<protocol::status_t> status{ protocol::success, protocol::status_labels };
        return status;
    }

    util::Status<protocol::status_t> write_start_message(buffers::Array &buffer)
    {
        util::Status<protocol::status_t> status{ protocol::success, protocol::status_labels };
        p_protocol.reset(new senack::Protocol{});
        p_protocol->write_start_message(buffer);
        return status;
    }
    util::Status<protocol::status_t> write_temperature_message(buffers::Array &buffer,
                                                             double value,
                                                             util::types::time_us_t ts)
    {
        util::Status<protocol::status_t> status{ protocol::success, protocol::status_labels };
        if (!messages.contains(temperature_f64_data_mid)) {
            messages[temperature_f64_data_mid] = new Message();
            status = p_protocol->create_data_temperature_double(messages[temperature_f64_data_mid]);
        }
        Message *m = messages[temperature_f64_data_mid];
        status = p_protocol->update_data_temperature_double(value, ts, m);
        m->serialize(buffer);
        return status;
    }
};  // class Parser
}  // namespace senack

#endif  // SENACK_PARSER_H
