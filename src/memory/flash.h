#ifndef MEMORY_FLASH_H
#define MEMORY_FLASH_H

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <string>

#include "io/spi.h"
#include "ncl/buffers/ArraySlice.h"
#include "ncl/logging/logging.h"
#include "ncl/util/Keystore.h"
#include "ncl/util/Status.h"

namespace memory {

enum status_t {
    okay_ = -127,
    success = 0,
    err_generic_fail,
    err_init_fail,
    err_not_init_fail,
    err_already_init_fail,
    err_read_fail,
    err_invalid_cmd_fmt,
    err_buf_capacity_exceeded,
    err_null_args,
    err_empty_write,
    err_not_initialized_fail,
    err_interface_error,
    err_not_implemented,
};  // enum status_t

const std::map<status_t, std::string> status_labels = {
    { success, "memory success" },
    { err_generic_fail, "memory generic error" },
    { err_init_fail, "memory initialization error" },
    { err_not_init_fail, "memory not initialized error" },
    { err_already_init_fail, "memory already initialized error" },
    { err_read_fail, "memory read error" },
    { err_invalid_cmd_fmt, "memory cmd format is invalid" },
    { err_buf_capacity_exceeded, "memory buffer capacity exceeded" },
    { err_null_args, "memory buffer for args is not allowed to be null" },
    { err_empty_write, "memory write error, empty write attempted" },
    { err_not_initialized_fail, "memory not initialized error" },
    { err_interface_error, "memory error with interface" },
    { err_not_implemented, "memory error not yet implemented" },
};  // map<> status_labels

using Status = util::Status<status_t>;

using addr_t = std::uint32_t;

enum instruction_t {
    read = 0x03,
    hs_read = 0x0b,
    erase_4k = 0x20,
    erase_32k = 0x52,
    erase_64k = 0xd8,
    erase_full = 0x60,
    erase_full_2 = 0xc7,
    write_byte = 0x02,
    aai = 0xad,
    read_status = 0x05,
    enable_write_status = 0x50,
    write_status = 0x01,
    write_enable = 0x06,
    write_disable = 0x04,
    read_id = 0x90,
    read_id_2 = 0xab,
    read_jid = 0x9f,
    enable_so = 0x70,
    disable_so = 0x80,
};  // enum instruction_t

enum status_reg_mask {
    busy_mask = 0x1 << 0,
    write_en_mask = 0x1 << 1,
    block_protect_0_mask = 0x1 << 2,
    block_protect_1_mask = 0x1 << 3,
    block_protect_2_mask = 0x1 << 4,
    block_protect_3_mask = 0x1 << 5,
    auto_address_inc_mask = 0x1 << 6,
    block_protect_mask = 0x1 << 7,
};  // enum status_reg_mask

static const uint status_bits_count = 8;
static const std::size_t max_data_len = 2;

std::string status_reg_label[status_bits_count] = {
    "busy",
    "write_en",
    "block_protect_0",
    "block_protect_1",
    "block_protect_2",
    "block_protect_3",
    "auto_address_inc",
    "block_protect",
};

class Flash {
    const io::SpiPins spi_pins{ .cs = 17,      // chip select
                                .sck = 18,     // clock
                                .mosi = 19,    // tx
                                .miso = 16 };  // rx
    const uint baud = 25 * 1000000;
    const io::SpiFormat spi_fmt{ .bits_count = 8,
                                 .clock_polarity = SPI_CPOL_1,  // set polarity and phase to capture
                                 .clock_phase = SPI_CPHA_1,     // on the rising edge
                                 .bits_order = SPI_MSB_FIRST };
    const std::uint8_t dummy_byte = 0;
    struct Members {
        bool has_initialized = false;
    } m;

   public:
    io::SpiInterface spi_itf;
    Flash() : spi_itf(spi_pins, baud, spi0, spi_fmt, dummy_byte)
    {
    }

    ~Flash()
    {
        if (m.has_initialized) {
            deinit();
        }
    }

    bool is_initialized()
    {
        return m.has_initialized;
    }

    Status init()
    {
        Status status{ status_t::success, status_labels };
        if (!m.has_initialized) {
            if (spi_itf.init().is_error()) {
                status = status_t::err_init_fail;
                LOG_E("err_init_fail");
            }
        } else {
            status = err_already_init_fail;
            LOG_E("err_already_init_fail");
        }
        m.has_initialized = status.is_success();
        return status;
    }

    Status deinit()
    {
        Status status{ status_t::success, status_labels };
        if (m.has_initialized) {
            if (spi_itf.deinit().is_error()) {
                status = status_t::err_interface_error;
                LOG_E("err_init_fail");
            }

        } else {
            status = status_t::err_not_initialized_fail;
        }
        m.has_initialized = false;
        return status;
    }
    inline Status set(std::uint8_t cmd, std::uint8_t *args, const std::size_t args_len)
    {
        const uint max_write_len = 6;
        const uint write_len = args_len + 1;
        std::uint8_t write_buf[max_write_len];
        Status status{ status_t::success, status_labels };
        if (write_len > max_write_len) {
            status = status_t::err_invalid_cmd_fmt;
        }
        if (status.is_success() && !m.has_initialized) {
            status = status_t::err_not_init_fail;
        }
        if (status.is_success()) {
            if (args_len > 0 && nullptr == args) {
                status = status_t::err_null_args;
            }
        }
        if (status.is_success()) {
            //char log_buf[256];
            write_buf[0] = cmd;
            if (args_len > 0) {
                std::copy_n(args, args_len, write_buf + 1);
                //util::print_as_hex(log_buf, sizeof(log_buf), args, args_len);
                //LOG_D("args_buf: %s", log_buf);
            }
            //util::print_as_hex(log_buf, sizeof(log_buf), write_buf, write_len);
            //LOG_D("write_buf: %s", log_buf);
        }
        if (status.is_success() /* && cmd != instruction_t::aai*/) {
            if (spi_itf.chip_enable(true).is_success()) {
                io::Status io_status = spi_itf.write(write_buf, write_len);
                if (io_status.is_error()) {
                    status = status_t::err_read_fail;
                }
            } else {
                status = status_t::err_read_fail;
            }
            spi_itf.chip_enable(false);
        }
        return status;
    }
    inline Status read(std::uint8_t cmd,
                       std::uint8_t *args,
                       const std::size_t args_len,
                       std::uint8_t *buf,
                       const std::size_t buf_len,
                       ssize_t &read_count)
    {
        const uint max_write_len = 5;
        const uint write_len = args_len + 1;
        std::uint8_t write_buf[max_write_len];
        Status status{ status_t::success, status_labels };
        read_count = 0;
        if (write_len > max_write_len) {
            status = status_t::err_invalid_cmd_fmt;
        }
        if (status.is_success() && !m.has_initialized) {
            LOG_E("error: err_not_init_fail");
            status = status_t::err_not_init_fail;
        }
        if (status.is_success()) {
            if (args_len > 0 && nullptr == args) {
                LOG_E("error: err_null_args");
                status = status_t::err_null_args;
            }
        }
        if (status.is_success()) {
            char log_buf[256];
            write_buf[0] = cmd;
            if (args_len > 0) {
                std::copy_n(args, args_len, write_buf + 1);
                util::print_as_hex(log_buf, sizeof(log_buf), args, args_len);
                //LOG_D("args_buf: %s", log_buf);
            }
            util::print_as_hex(log_buf, sizeof(log_buf), write_buf, write_len);
            //LOG_D("write_buf: %s", log_buf);
        }
        if (status.is_success()) {
            spi_itf.chip_enable(true);
            io::Status io_status = spi_itf.write(write_buf, write_len);
            if (io_status.is_success()) {
                io_status = spi_itf.read(buf, buf_len, read_count);
            } else {
                LOG_E("error: err_read_fail");
                status = status_t::err_read_fail;
            }
            spi_itf.chip_enable(false);
        }
        //LOG_D("read count: %zu", read_count);
        return status;
    }

    Status read_jedec(std::uint8_t *buf, const std::size_t buf_len, ssize_t &read_count)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::read_jid;
        const std::uint8_t min_buf_len = 3;
        if (min_buf_len > buf_len) {
            status = status_t::err_buf_capacity_exceeded;
            return status;
        }
        if (spi_itf.chip_enable(true).is_error()) {
            status = status_t::err_interface_error;
            return status;
        }
        status = read(cmd, nullptr, 0, buf, buf_len, read_count);
        spi_itf.chip_enable(false);
        return status;
    }

    Status read_id(std::uint8_t *buf,
                   const std::size_t buf_len,
                   const addr_t address,
                   ssize_t &read_count)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::read_id;
        const uint min_buf_len = 1;
        const uint addr_len = 3;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        std::uint8_t *addr_p = reinterpret_cast<std::uint8_t *>(&addr_r);
        if (min_buf_len > buf_len) {
            status = status_t::err_buf_capacity_exceeded;
            return status;
        }
        status = read(cmd, addr_p, addr_len, buf, buf_len, read_count);
        return status;
    }

    Status read_status(std::uint8_t &stat)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::read_status;
        ssize_t read_count = 0;
        status = read(cmd, nullptr, 0, &stat, 1, read_count);
        return status;
    }

    void print_status(std::uint8_t status)
    {
        LOG_I("status: %02x", status);
        for (uint i = 0; i < status_bits_count; ++i) {
            std::uint8_t mask = 0x1 << i;
            LOG_I("status bit[%u]: %s=%u", i, status_reg_label[i].c_str(), (status & mask) >> i);
        }
    }

    Status read_data(std::uint8_t *buf,
                     const std::size_t buf_len,
                     const addr_t address,
                     ssize_t &read_count)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::read;
        const uint min_buf_len = 1;
        const uint addr_len = 3;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        std::uint8_t *addr_p = reinterpret_cast<std::uint8_t *>(&addr_r);
        if (min_buf_len > buf_len) {
            status = status_t::err_buf_capacity_exceeded;
            return status;
        }
        status = read(cmd, addr_p, addr_len, buf, buf_len, read_count);
        return status;
    }

    Status read_data(buffers::Array &buf, const size_t expected_count, const addr_t address)
    {
        Status status{ status_t::success, status_labels };
        ssize_t read_count = 0;
        if (expected_count > buf.remaining_space()) {
            status = status_t::err_buf_capacity_exceeded;
            return status;
        }
        status = read_data(buf.data_at(buf.len()), expected_count, address, read_count);
        buf.len(buf.len() + read_count);
        return status;
    }

    Status fast_read_data(std::uint8_t *buf,
                          const std::size_t buf_len,
                          const addr_t address,
                          ssize_t &read_count)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::read;
        const uint min_buf_len = 1;
        const uint addr_len = 4;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        std::uint8_t *addr_p = reinterpret_cast<std::uint8_t *>(&addr_r);
        if (min_buf_len > buf_len) {
            status = status_t::err_buf_capacity_exceeded;
            return status;
        }
        status = read(cmd, addr_p, addr_len, buf, buf_len, read_count);
        return status;
    }

    Status enable_write()
    {
        const std::uint8_t cmd = instruction_t::write_enable;
        return set(cmd, nullptr, 0);
    }

    Status disable_write()
    {
        const std::uint8_t cmd = instruction_t::write_disable;
        Status status = set(cmd, nullptr, 0);
        busy_wait_us(10);
        return status;
    }

    Status set_status(bool wr_e, bool bp0, bool bp1, bool bp2, bool bp3, bool bp)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::write_status;
        std::uint8_t stat = 0;
        if (wr_e) {
            stat = stat | status_reg_mask::write_en_mask;
        }
        if (bp0) {
            stat = stat | status_reg_mask::block_protect_0_mask;
        }
        if (bp1) {
            stat = stat | status_reg_mask::block_protect_1_mask;
        }
        if (bp2) {
            stat = stat | status_reg_mask::block_protect_2_mask;
        }
        if (bp3) {
            stat = stat | status_reg_mask::block_protect_3_mask;
        }
        if (bp) {
            stat = stat | status_reg_mask::block_protect_mask;
        }
        status = enable_write();
        if (status.is_success()) {
            status = set(cmd, &stat, 1);
        }
        disable_write();
        return status;
    }

    Status erase_4k(addr_t address)
    {
        Status status{ status_t::success, status_labels };
        // erase will take a max of 25ms
        const std::uint8_t cmd = instruction_t::erase_4k;
        const uint addr_len = 3;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        status = enable_write();
        if (status.is_success()) {
            status = set(cmd, reinterpret_cast<uint8_t *>(&addr_r), addr_len);
        }
        disable_write();
        return status;
    }

    Status erase_32k(addr_t address)
    {
        Status status{ status_t::success, status_labels };
        // erase will take a max of 25ms
        const std::uint8_t cmd = instruction_t::erase_32k;
        const uint addr_len = 3;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        status = enable_write();
        if (status.is_success()) {
            status = set(cmd, reinterpret_cast<uint8_t *>(&addr_r), addr_len);
        }
        disable_write();
        return status;
    }

    Status erase_64k(addr_t address)
    {
        Status status{ status_t::success, status_labels };
        // erase will take a max of 25ms
        const std::uint8_t cmd = instruction_t::erase_64k;
        const uint addr_len = 3;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        status = enable_write();
        if (status.is_success()) {
            status = set(cmd, reinterpret_cast<uint8_t *>(&addr_r), addr_len);
        }
        disable_write();
        return status;
    }

    Status erase_full()
    {
        Status status{ status_t::success, status_labels };
        // erase will take a max of 50ms
        const std::uint8_t cmd = instruction_t::erase_full;
        status = set_status(1, 0, 0, 0, 0, 0);
        if (status.is_success()) {
            status = enable_write();
        }
        if (status.is_success()) {
            status = set(cmd, nullptr, 0);
        }
        disable_write();
        return status;
    }

    Status set_byte(addr_t address, std::uint8_t data)
    {
        // write will take a max of 10us
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::write_byte;
        const uint args_len = 4;
        std::uint32_t addr_r = util::htobe(address) >> 8;
        addr_r = addr_r | (data << 24);
        std::uint8_t *args = reinterpret_cast<std::uint8_t *>(&addr_r);
        status = enable_write();
        if (status.is_success()) {
            status = set(cmd, args, args_len);
        }
        disable_write();
        return status;
    }

    Status blocking_set_byte(addr_t address, std::uint8_t data)
    {
        Status status = set_byte(address, data);
        busy_wait_us(10);
        return status;
    }

    Status set_aai(addr_t address,
                   std::uint8_t *data,
                   std::size_t data_len,
                   std::size_t &write_count)
    {
        Status status{ status_t::success, status_labels };
        // write will take a max of 10us
        const std::uint8_t cmd = instruction_t::aai;
        const uint addr_len = 3;
        if (0 == data_len) {
            status = status_t::err_empty_write;
            return status;
        }
        write_count = std::min(data_len, max_data_len);
        const uint args_len = write_count + addr_len;
        std::uint8_t args[args_len];
        std::uint32_t addr_r = util::htobe(address) >> 8;
        std::copy_n(reinterpret_cast<uint8_t *>(&addr_r), addr_len, args);
        std::copy_n(data, write_count, args + addr_len);
        //LOG_D("write_count: %d", write_count);
        status = set(cmd, args, args_len);
        return status;
    }

    Status aai_write(std::uint8_t *data, std::size_t data_len, std::size_t &write_count)
    {
        Status status{ status_t::success, status_labels };
        const std::uint8_t cmd = instruction_t::aai;
        std::uint8_t args[max_data_len];
        const uint args_len = std::min(data_len, max_data_len);
        if (0 == data_len) {
            status = status_t::err_empty_write;
            return status;
        }
        std::copy_n(data, args_len, args);
        write_count = args_len;
        //LOG_D("write_count: %d", write_count);
        return set(cmd, args, args_len);
    }

    Status blocking_write(addr_t address,
                          std::uint8_t *data,
                          std::size_t data_len,
                          std::size_t &write_count)
    {
        Status status{ status_t::success, status_labels };
        if (nullptr == data) {
            status = status_t::err_null_args;
            return status;
        }
        if (data_len == 0) {
            status = status_t::err_empty_write;
            return status;
        }
        if (data_len == 1) {
            status = blocking_set_byte(address, data[0]);
            return status;
        }
        if (address % 2 == 1) {
            // data written from aai starts at even adresses, regardless of address input
            status = blocking_set_byte(address, data[0]);
            if (status.is_error()) {
                return status;
            } else {
                write_count = 1;
            }
        }
        std::size_t temp_write_count = 0;
        status = enable_write();
        if (status.is_success()) {
            status = set_aai(
              address + write_count, data + write_count, data_len - write_count, temp_write_count);
            write_count += temp_write_count;
            while (status.is_success()) {
                busy_wait_us(10);
                if (status.is_success() && (data_len - write_count) > 1) {
                    status
                      = aai_write(data + write_count, data_len - write_count, temp_write_count);
                    write_count += temp_write_count;
                } else if (status.is_success() && ((data_len - write_count) == 1)) {
                    busy_wait_us(10);
                    disable_write();
                    status = blocking_set_byte(address + write_count, data[write_count]);
                    write_count += 1;
                } else {
                    busy_wait_us(10);
                    break;
                }
            }
        }
        if (status.is_error()) {
            LOG_E("error: %s", status.as_string().c_str());
        }
        disable_write();
        return status;
    }

    Status blocking_write(addr_t address, buffers::Array &buf)
    {
        Status status{ status_t::success, status_labels };
        if (buf.remaining_data() <= 0) {
            status = err_empty_write;
            return status;
        }
        std::size_t write_count = 0;
        status = blocking_write(address, buf.data_at(buf.pos()), buf.remaining_data(), write_count);
        buf.pos(buf.pos() + write_count);
        return status;
    }

    Status blocking_erase_4k(addr_t address)
    {
        Status status{ status_t::success, status_labels };
        status = erase_4k(address);
        bool is_busy = true;
        while (status.is_success() && is_busy) {
            std::uint8_t stat = 0;
            busy_wait_us(1000);
            status = read_status(stat);
            is_busy = stat & status_reg_mask::busy_mask;
        }
        return status;
    }

    Status blocking_erase_32k(addr_t address)
    {
        Status status{ status_t::success, status_labels };
        status = erase_32k(address);
        bool is_busy = true;
        while (status.is_success() && is_busy) {
            std::uint8_t stat = 0;
            busy_wait_us(1000);
            status = read_status(stat);
            is_busy = stat & status_reg_mask::busy_mask;
        }
        return status;
    }

    Status blocking_erase_64k(addr_t address)
    {
        Status status{ status_t::success, status_labels };
        status = erase_64k(address);
        bool is_busy = true;
        while (status.is_success() && is_busy) {
            std::uint8_t stat = 0;
            busy_wait_us(1000);
            status = read_status(stat);
            is_busy = stat & status_reg_mask::busy_mask;
        }
        return status;
    }

    Status blocking_erase_full()
    {
        Status status{ status_t::success, status_labels };
        status = erase_full();
        bool is_busy = true;
        while (status.is_success() && is_busy) {
            std::uint8_t stat = 0;
            busy_wait_us(1000);
            status = read_status(stat);
            is_busy = stat & status_reg_mask::busy_mask;
        }
        return status;
    }

};  // class Flash

#if 1
// move to new file

class FlashStorageHandler : public util::keystore::StorageHandler {
    Flash flash;
    struct Members {
        addr_t address;
        bool has_initialized;
    } m;

    public:

    using util::keystore::StorageHandler::write;
    using util::keystore::StorageHandler::read;

    FlashStorageHandler() : flash(), m(Members{ .address = 0 })
    {
    }

    FlashStorageHandler(addr_t initial_addr) : flash(), m(Members{ .address = initial_addr })
    {
    }

    virtual ~FlashStorageHandler()
    {
        if(m.has_initialized)
        {
            close();
        }
    }

    virtual util::keystore::Status init()
    {
        util::keystore::Status status{ util::keystore::status_t::success,
                                       util::keystore::status_labels };
        if (flash.init().is_error()) {
            status = util::keystore::status_t::err_storage_init_failed;
        }
        if (status.is_success())
        {
            m.has_initialized = true;
        }
        return status;
    }

    virtual util::keystore::Status close()
    {
        util::keystore::Status status{ util::keystore::status_t::success,
                                       util::keystore::status_labels };
        if(!m.has_initialized)
        {
            status = util::keystore::status_t::err_not_initialized_fail;
        }
        if (status.is_success() && flash.deinit().is_error()) {
            status = util::keystore::status_t::err_storage_deinit_failed;
        }
        m.has_initialized = false;
        return status;
    }

    virtual util::keystore::Status write(uint8_t *p_data, std::size_t len)
    {
        util::keystore::Status status{ util::keystore::status_t::success,
                                       util::keystore::status_labels };
        std::size_t write_count = 0;
        if(!m.has_initialized)
        {
            status = util::keystore::status_t::err_not_initialized_fail;
        }
        if (status.is_success() && flash.blocking_write(m.address, p_data, len, write_count).is_error()) {
            status = util::keystore::status_t::err_storage_write_failed;
        }
        if(status.is_success())
        {
            m.address += write_count;
        }
        return status;
    }

    virtual util::keystore::Status read(uint8_t *p_data, std::size_t capacity, ssize_t &read_count)
    {
        util::keystore::Status status{ util::keystore::status_t::success,
                                       util::keystore::status_labels };
        if(!m.has_initialized)
        {
            status = util::keystore::status_t::err_not_initialized_fail;
        }
        if (status.is_success() && flash.read_data(p_data, capacity, m.address, read_count).is_error()) {
            status = util::keystore::status_t::err_storage_read_failed;
        }
        if(status.is_success())
        {
            m.address += read_count;
        }
        return status;
    }

    virtual util::keystore::Status clear()
    {
        util::keystore::Status status{ util::keystore::status_t::success,
                                       util::keystore::status_labels };
        if(!m.has_initialized)
        {
            status = util::keystore::status_t::err_not_initialized_fail;
        }
        if (status.is_success() && flash.blocking_erase_full().is_error()) {
            status = util::keystore::status_t::err_storage_erase_failed;
        }
        if(status.is_success())
        {
            m.address = 0;
        }
        return status;
    }

    std::size_t pos()
    {
        return m.address;
    }

    memory::Flash *get_io()
    {
        return &flash;
    }

    bool is_initialized()
    {
        return m.has_initialized;
    }
};  // class FlashStorageHandler
#endif  // if 0

}  // namespace memory

#endif  // define MEMORY_FLASH_H
