#ifndef PROTOCOL_PROTOCOL_H
#define PROTOCOL_PROTOCOL_H
#include <map>
#include <stdint.h>
#include <string>

#include "ncl/util/Crc32.h"
#include "ncl/util/Status.h"

namespace protocol {
using protocol_id_t = uint32_t;
using message_id_t = uint64_t;
using crc32_t = uint32_t;

struct Version {
    uint8_t major;
    uint8_t minor;
    uint8_t patch;
    uint8_t build;
};

/**
 * arg_t - u is unsigned int, i is signed int, a is array, utf8 is an array of utf8 characters
 */
enum arg_t {
    u16,
    u32,
    u64,
    i16,
    i32,
    i64,
    f32,
    f64,
    u8a,
    u16a,
    u32a,
    u64a,
    i16a,
    i32a,
    i64a,
    f32a,
    f64a,
    utf8,
};

enum status_t {
    fail_status = -127,
    null_pointer_error,
    protocol_id_mismatch_error,
    buffer_capacity_exceeded_error,
    buffer_error,
    success = 0,
    message_incomplete_okay,
};

const std::map<status_t, std::string> status_labels = {
    { fail_status, "fail_status" },
    { null_pointer_error, "null_pointer_error" },
    { buffer_capacity_exceeded_error, "buffer_capacity_exceeded_error" },
    { protocol_id_mismatch_error, "protocol_id_mismatch_error" },
    { success, "success" },
    { message_incomplete_okay, "message_incomplete_okay" },
};

/**
 * Message - contains message id, and a set of arguments for the message
 */
struct Message {
   public:
    struct Members {
        message_id_t message_id;
        crc32_t crc32;
    } m;

    virtual ~Message()
    {
    }

    virtual size_t calc_size()
    {
        return sizeof(message_id_t) + calc_payload_size() + sizeof(crc32_t);
    }

    virtual size_t calc_payload_size()
    {
        return 0;
    }
    virtual util::Status<status_t> serialize_body(buffers::Array &buffer)
    {
        util::Status<status_t> status(success, status_labels);
        return status;
    }

    virtual util::Status<status_t> serialize(buffers::Array &buffer)
    {
        util::Status<status_t> status(success, status_labels);
        if (calc_size() > buffer.remaining_space()) {
            status = buffer_capacity_exceeded_error;
            return status;
        }
        auto buf_status = buffer.push_type(m.message_id);
        if (!status.is_error() && !buf_status.is_error()) {
            status = serialize_body(buffer);
        }
        if (!status.is_error() && !buf_status.is_error()) {
            util::Crc32 crc_gen{};
            m.crc32 = crc_gen.calc(reinterpret_cast<uint8_t *>(buffer.data()), buffer.len());
            buf_status = buffer.push_type(m.crc32);
        }
        if (buf_status.is_error()) {
            LOG_D("Array error:%s", buf_status.as_string().c_str());
            status = buffer_error;
        }
        return status;
    }
};  // class Message

struct ProtocolArgs {
    protocol_id_t id;
    Version version;
}; //struct ProtocolArgs

static util::Status<status_t> build_start_message(protocol_id_t prot_id,
                                                  Version version,
                                                  Message &message)
{
    util::Status<status_t> status{ success, status_labels };
    union MessageIdUnion {
        message_id_t message_id;
        ProtocolArgs args;
        uint8_t buf[sizeof(message_id_t)];
    } message_header;
    message_header.args.id = prot_id;
    message_header.args.version = version;

    util::Crc32 crc_gen{};
    message.m.message_id = message_header.message_id;
    message.m.crc32 = crc_gen.calc(message_header.buf, sizeof(message_header.buf));
    return status;
}

class ProtocolCallback {
   public:
    virtual void operator()(Message message) = 0;
}; //class ProtocolCallback

class Protocol {
    struct Members {
        protocol_id_t id;
        Version version;
    } m;
    virtual bool is_message_complete(buffers::ArrayBuffer &message)
    {
        return false;
    }
    virtual std::optional<size_t> get_payload_len(buffers::ArrayBuffer &message)
    {
        return -1;
    }
    virtual util::Status<status_t> handle_message(buffers::ArrayBuffer &message)  // callback
    {
        util::Status<status_t> status{ success, status_labels };
        status = fail_status;
        return status;
    }

   protected:
    Protocol(protocol_id_t id, uint8_t major, uint8_t minor, uint8_t patch, uint8_t build)
        : m(Members{ .id = id,
                     .version{ .major = major, .minor = minor, .patch = patch, .build = build } })
    {
    }

   public:
    util::Status<status_t> check_message(buffers::ArrayBuffer &message)
    {
        util::Status<status_t> status{ success, status_labels };
        if (is_message_complete(message)) {
            status = handle_message(message);
            if (status.is_error()) {
                // TODO: reset connection on bad parsing state
            }
        }
        return status;
    }

    util::Status<status_t> write_start_message(buffers::Array &buffer)
    {
        util::Status<status_t> status{ success, status_labels };
        Message message{};
        build_start_message(m.id, m.version, message);
        message.serialize(buffer);
        return status;
    }
}; //class Protocol

}  // namespace protocol
#endif  // PROTOCOL_PROTOCOL_H
