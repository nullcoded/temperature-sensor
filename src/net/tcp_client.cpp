#include "net/tcp_client.h"

#include "lwip/dns.h"
#include "lwip/ip_addr.h"
#include "ncl/logging/logging.h"

void net::TcpClient::dns_found(const char *hostname, const ip_addr_t *p_ipaddr, void *vp_tcp_client)
{
    LOG_D("%s", "dns");
    TcpClient &tcp_client = *static_cast<TcpClient *>(vp_tcp_client);
    tcp_client.state.print();
    if (NULL != p_ipaddr) {
        tcp_client.m.host_ip = *p_ipaddr;
        tcp_client.state.set(state_host_resolved);

        LOG_I("HOST IP %lu.%lu.%lu.%lu",
              (tcp_client.m.host_ip.addr & 0xff),
              ((tcp_client.m.host_ip.addr >> 8) & 0xff),
              ((tcp_client.m.host_ip.addr >> 16) & 0xff),
              (tcp_client.m.host_ip.addr >> 24));
        return;
    }
    tcp_client.state.set(state_error_host_lookup_failed);
    LOG_E("%s", "state_error_host_lookup_failed");
}

void net::TcpClient::init_host_lookup()
{
    if (m.host_ip.addr == 0) {
        state.set(state_host_lookup);
        dns_gethostbyname(m.host_str.c_str(), &m.host_ip, dns_found, this);
    } else {
        dns_found(m.host_str.c_str(), &m.host_ip, this);
    }
}

bool net::TcpClient::poll()
{
    if(out_buffer.len() > 0)
    {
        switch (state.get())
        {
            case state_host_connected:
            case state_host_connected_data_recv:
            {
                err_t err
                  = tcp_write(m.p_tcp_pcb, out_buffer.data(), out_buffer.len(), TCP_WRITE_FLAG_COPY);
                out_buffer.print_as_hex();
                if (err == ERR_OK) {
                    tcp_output(m.p_tcp_pcb);
                    out_buffer.drain(out_buffer.len());
                } else {
                    LOG_D("err: %u", err);
                }
                break;
            }
            default:
                break;
        }
    }
    switch (state.get()) {
    case state_error_host_lookup_failed: {
        if (state.has_time_elapsed_us(retry_delay_us) && m.has_network_up) {
            init_host_lookup();
        }
        break;
    }
    case state_error_connection_failed: {
        if (state.has_time_elapsed_us(retry_delay_us)) {
            state.set(state_host_resolved);
        }
        break;
    }
    case state_host_resolved: {
        bool is_error = open();
        if (!is_error) {
            state.set(state_host_connecting);
        }
        break;
    }
    case state_host_on_connected: {
        if (p_tcp_event_callback) {
            (*p_tcp_event_callback)(connect_event);
            state.set(state_host_connected);
        }
        break;
    }
    case state_host_connected_data_recv: {
        if (p_tcp_event_callback) {
            (*p_tcp_event_callback)(receive_data_event);
            state.set(state_host_connected);
        }
        break;
    }

    default:
        break;
    }
    return false;
}

bool net::TcpClient::open()
{
    LOG_D("%s", "con");
    state.print();
    if (nullptr == m.p_tcp_pcb) {
        m.p_tcp_pcb = (tcp_new_ip_type(IP_GET_TYPE(&m.host_ip)));
        if (nullptr == m.p_tcp_pcb) {
            LOG_E("%s", "unable to create tcp_pcb");
            return true;
        }
    }

    cyw43_arch_lwip_begin();
    tcp_arg(m.p_tcp_pcb, this);
    tcp_err(m.p_tcp_pcb, tcp_client_err);
    tcp_poll(m.p_tcp_pcb, tcp_client_poll, 10);
    tcp_recv(m.p_tcp_pcb, tcp_client_recv);
    err_t err = tcp_connect(m.p_tcp_pcb, &m.host_ip, m.port, tcp_client_connected);
    LOG("tcp_connect: %s:%u, err:%d", ip4addr_ntoa(&m.host_ip), m.port, err);
    cyw43_arch_lwip_end();

    return ERR_OK != err;
}
