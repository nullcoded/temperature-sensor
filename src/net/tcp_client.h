#ifndef NET_TCP_CLIENT_H
#define NET_TCP_CLIENT_H

#include <cstdint>
#include <memory>
#include <string>

#include "lwip/tcp.h"
#include "ncl/util/util.h"
#include "net/wifi.h"
#include "senack/Parser.h"
#include "senack/Protocol.h"

namespace net {

enum tcp_event_t { connect_event, disconnect_event, receive_data_event };

const std::map<tcp_event_t, std::string> tcp_event_labels = {
    { connect_event, "connect_event" },
    { disconnect_event, "disconnect_event" },
    { receive_data_event, "receive_data_event" },
};

class OnTcpEventCallback {
   public:
    virtual void operator()(tcp_event_t event) = 0;
};
/**
 *  tcp_client - based on lwip
 */
class TcpClient {
    const uint64_t retry_delay_us = 10;
    class TcpOnNetEventCallback : public net::OnNetEventCallback {
        TcpClient &client;

       public:
        TcpOnNetEventCallback(TcpClient *p) : client(*p)
        {
        }

        virtual void operator()(bool connected)
        {
            client.m.has_network_up = connected;
            if (connected) {
                client.init_host_lookup();
            }
        }
    };

    enum status_t { status_error = -128, status_success = 0, status_ok };
    enum state_t {
        state_error_connection_failed = -128,
        state_error_connection_timedout,
        state_error_host_lookup_failed,
        state_init = 0,
        state_host_lookup,
        state_host_resolved,
        state_host_connecting,
        state_host_on_connected,
        state_host_connected,
        state_host_connected_data_recv,
    };
    const std::map<state_t, std::string> state_label = {
        { state_error_connection_failed, "state_error_connection_failed" },
        { state_error_connection_timedout, "state_error_connection_timedout" },
        { state_error_host_lookup_failed, "state_error_host_lookup_failed" },
        { state_init, "state_init" },
        { state_host_lookup, "state_host_lookup" },
        { state_host_resolved, "state_host_resolved" },
        { state_host_connecting, "state_host_connecting" },
        { state_host_on_connected, "state_host_on_connected" },
        { state_host_connected, "state_host_connected" },
        { state_host_connected_data_recv, "state_host_connected_data_recv" },
    };
    struct Members {
        std::string host_str;
        ip_addr_t host_ip;
        uint16_t port;
        tcp_pcb *p_tcp_pcb;
        bool has_network_up;
    } m;
    util::State<state_t> state;
    TcpOnNetEventCallback on_net_event_callback;
    buffers::ArrayBuffer out_buffer;
    buffers::ArrayBuffer in_buffer;
    OnTcpEventCallback *p_tcp_event_callback;

    static void dns_found(const char *hostname, const ip_addr_t *ipaddr, void *vp_tcp_client);

   public:
    TcpClient(std::string host_str, uint16_t port)
        : m(Members{ .host_str = host_str, .port = port }),
          state(state_init, state_label),
          on_net_event_callback(this),
          out_buffer(4 * util::kb),
          in_buffer(4 * util::kb),
          p_tcp_event_callback(nullptr)
    {
    }

    void set_event_callback(OnTcpEventCallback *p_cb)
    {
        p_tcp_event_callback = p_cb;
    }

    void lock()
    {
        cyw43_arch_lwip_begin();
    }
    void unlock()
    {
        cyw43_arch_lwip_end();
    }

    buffers::ArrayBuffer &get_write_buffer()
    {
        lock();
        return out_buffer;
    }
    void finalize_write_buffer()
    {
        unlock();
    }
    bool poll();
    void init_host_lookup();
    bool open();
    net::OnNetEventCallback *get_net_callback()
    {
        return &on_net_event_callback;
    }

    static err_t tcp_client_connected(void *arg, struct tcp_pcb *tpcb, err_t err)
    {
        TcpClient &client = *reinterpret_cast<TcpClient *>(arg);
        if (ERR_OK == err) {
            client.state.set(state_host_on_connected);
        } else {
            LOG("on connected error: %u", err);
            client.state.set(state_error_connection_failed);
        }
        return err;
    }
    static void tcp_client_err(void *arg, err_t err)
    {
        LOG_E("err: %d", err);
        TcpClient &client = *reinterpret_cast<TcpClient *>(arg);
        tcp_close(client.m.p_tcp_pcb);
        client.m.p_tcp_pcb = nullptr;
        client.state.set(state_host_resolved);
    }
    static err_t tcp_client_poll(void *arg, struct tcp_pcb *tpcb)
    {
        TcpClient &client = *reinterpret_cast<TcpClient *>(arg);
        err_t err = ERR_OK;
        LOG_D("tcp_client_poll: %s", client.state.as_string().c_str());
        return err;
    }
    static err_t tcp_client_recv(void *arg, struct tcp_pcb *tcpb, struct pbuf *p, err_t err)
    {
        TcpClient &client = *reinterpret_cast<TcpClient *>(arg);
        client.lock();
        size_t transfered_size = 0;
        if(!err && p->tot_len > 0)
        {
            for (struct pbuf *q=p; q!=NULL; q=q->next)
            {
                util::StatusInterface array_status = client.in_buffer.push(reinterpret_cast<std::uint8_t *>(q->payload), q->len);
                if(array_status.is_error())
                {
                    LOG_D("in buffer is full: data lost, %u bytes", p->tot_len-transfered_size);
                    err = ERR_BUF;
                }
                else
                {
                    transfered_size+=q->len;
                }
            }
        }
        client.unlock();
        return err;
    }

};  // class TcpClient

}  // namespace net
#endif  // #ifndef NET_TCP_CLIENT_H
