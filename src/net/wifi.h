#ifndef NET_WIFI_H
#define NET_WIFI_H

#include <string>

#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"
#include "ncl/util/util.h"
#include "ncl/util/State.h"

namespace net {

class OnNetEventCallback
{
    public:
    virtual void operator()(bool connected)=0;
}; // class OnNetEventCallback

namespace WifiDriver {
    const uint64_t poll_rate_us = util::us_per_s/10; 
    bool init();
    bool has_initialized();
    void deinit();
    void poll();
}; // namespace WifiDriver


class Wifi {

    const uint64_t min_timeout_s = 10;
    const uint64_t max_timeout_s = 60*10;

    enum state_t {
        connect_timeout_state=-128,
        connect_error_state,
        init_state=0,
        idle_state,
        connecting_state,
        connected_state,
        disconnected_state,
    };

    const std::map<state_t, std::string> state_labels = {
        {connect_timeout_state, "connect_timeout_state"},
        {connect_error_state, "connect_error_state"},
        {init_state, "init_state"},
        {idle_state, "idle_state"},
        {connecting_state, "connecting_state"},
        {connected_state, "connected_state"},
        {disconnected_state, "disconnected_state"},
    };

    struct Members {
        std::string ssid;
        std::string pw;
        uint32_t auth;
        uint32_t timeout_ms;
        uint32_t delay_us;
    } m;
    util::State<state_t> state;
    OnNetEventCallback *p_on_event_cb;

    inline Wifi(Members &&m_) : m(m_), state(init_state, state_labels)
    {
        WifiDriver::init();
        cyw43_arch_enable_sta_mode();
        state.set(idle_state);
    }
    //disallow copies
    Wifi(const Wifi&) = delete;
    Wifi& operator=(const Wifi&) = delete;
    void update_delay();

    public:
    Wifi(std::string ssid, std::string pw, uint32_t auth, uint32_t timeout)
        : Wifi(Members{ .ssid=ssid, .pw=pw, .auth=auth, .timeout_ms=timeout})
    {
        const uint64_t max_timeout_ms = max_timeout_s*util::ms_per_s;
        const uint64_t min_timeout_ms = min_timeout_s*util::ms_per_s;
        if(m.timeout_ms < min_timeout_ms)
        {
            m.timeout_ms=min_timeout_ms;
        }
        if(m.timeout_ms > max_timeout_ms)
        {
            m.timeout_ms=max_timeout_ms;
        }
    }
    ~Wifi()
    {
        cyw43_arch_disable_sta_mode();
    }
    bool connect();
    bool disconnect();
    bool is_up();
    bool is_connected();
    bool poll();
    uint32_t get_ip();
    void set_callback(OnNetEventCallback *p_cb)
    {
        p_on_event_cb=p_cb;
    }

}; //class Wifi

} //namespace net
#endif // define NET_WIFI_H
