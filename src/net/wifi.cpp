#include "net/wifi.h"

#include "ncl/logging/logging.h"
#include "lwip/netif.h"

static bool driver_has_initialized = false;

/**
 * driver_init - return true if error.
 */
bool net::WifiDriver::init()
{
    if (has_initialized()) {
        LOG_D("%s", "attempted to reinit driver");
        return false;
    }
    LOG_D("%s", "cyw43_arch_init_with_country");
    int err_code = cyw43_arch_init_with_country(CYW43_COUNTRY_USA);
    LOG_D("err_code: %d", err_code);
    if (PICO_OK != err_code) {
        LOG_E("unable to init WiFi err:%d", err_code);
        return true;
    }
    driver_has_initialized = true;
    return false;
}

/**
 * driver_deinit -
 */
void net::WifiDriver::deinit()
{
    if (!has_initialized()) {
        LOG_D("%s", "attempting to deinit driver that was not initialized");
    }
    cyw43_arch_deinit();
    driver_has_initialized = false;
}

bool net::WifiDriver::has_initialized()
{
    return driver_has_initialized;
}

void net::WifiDriver::poll()
{
#if PICO_CYW43_ARCH_FREERTOS
    cyw43_arch_poll();
#endif // if PICO_CYW43_ARCH_FREERTOS
}

/**
 * Wifi::connect - return true if error
 */
bool net::Wifi::connect()
{
    if (is_connected()) {
        LOG_D("%s", "attempted to connect wifi while connected");
        return false;
    }
    state.set(connecting_state);
    LOG_D("%s", "set connecting_state");
    int err_code
      = cyw43_arch_wifi_connect_async(m.ssid.c_str(), m.pw.c_str(), m.auth);
    LOG_D("connect: err:%d", err_code);
    if (PICO_OK != err_code) {
        LOG_E("failed to connect to wifi err: %d", err_code);
        cyw43_wifi_leave(&cyw43_state, CYW43_ITF_STA);
        state.set(connect_error_state);
        LOG_D("%s", "set connect_error_state");
        return true;
    } else {

    }
    return false;
}

bool net::Wifi::disconnect()
{
    if(is_connected())
    {
        int ret=cyw43_wifi_leave(&cyw43_state, CYW43_ITF_STA);
        if(0==ret)
        {
            state.set(idle_state);
            LOG_D("%s", "set idle_state");
        }
        return ret;
    }
    LOG_D("%s", "attempted to disconnect while not connected");
    return true;
}

bool net::Wifi::is_connected()
{
    switch(state.get())
    {
        case connecting_state:
        case connected_state:
            return true;
        default:
            return false;
    }
}
bool net::Wifi::is_up()
{
    int status = cyw43_tcpip_link_status(&cyw43_state, CYW43_ITF_STA);
    return status == CYW43_LINK_UP;
}

uint32_t net::Wifi::get_ip()
{
    if(!is_up())
    {
        return 0;
    }
    return cyw43_state.netif->ip_addr.addr;
}

bool net::Wifi::poll()
{
    bool has_error=false;
    switch(state.get())
    {
        case connecting_state:
        {
            if(is_up())
            {
                if(p_on_event_cb)
                {
                    (*p_on_event_cb)(true);
                }
                state.set(connected_state);
                LOG_D("%s", "set connected_state");
                uint32_t local_ip = get_ip();

                LOG_I("IP %lu.%lu.%lu.%lu",
                       (local_ip & 0xff),
                       ((local_ip >> 8) & 0xff),
                       ((local_ip >> 16) & 0xff),
                       (local_ip >> 24));
            }
            else if (state.has_time_elapsed_us(m.timeout_ms*util::us_per_ms))
            {
                LOG_E("wifi timeout exceeded, %llu", state.elapsed_time_us());
                state.set(connect_timeout_state);
                LOG_D("%s", "set connect_timeout_state");
            }
            break;
        }
        case connected_state:
        {

            if(!is_up()) {
                state.set(disconnected_state);
                if(p_on_event_cb)
                {
                    (*p_on_event_cb)(false);
                }
            }
            break;
        }
        case disconnected_state:
        case connect_timeout_state:
        case connect_error_state:
        {
            if(state.has_time_elapsed_us(m.delay_us))
            {
                connect();
            }
            break;
        }
        default:
            //do nothing
            break;
    }
    WifiDriver::poll();
    return has_error;
}

void net::Wifi::update_delay()
{
    if(0==m.delay_us)
    {
        m.delay_us=m.timeout_ms*util::us_per_ms;
    }
    else
    {
        m.delay_us*=2;
        const uint64_t max_timeout_us = max_timeout_s*util::ms_per_s*util::us_per_ms;
        if(m.delay_us > max_timeout_us)
        {
            m.delay_us = max_timeout_us;
        }
    }
}
