#ifndef IO_SPI_H
#define IO_SPI_H

#include <cstddef>
#include <string>

#include "hardware/gpio.h"
#include "hardware/spi.h"
#include "ncl/logging/logging.h"
#include "ncl/util/Status.h"
#include "pico/stdlib.h"
#include "ncl/buffers/ArraySlice.h"
#include "io/io.h"

namespace io {

struct SpiPins {
    io::pin_t cs;    // chip select
    io::pin_t sck;   // clock
    io::pin_t mosi;  // tx
    io::pin_t miso;  // rx
};  // struct SpiPins

struct SpiFormat {
    uint bits_count;            // number of bits per transfer [4..16]
    spi_cpol_t clock_polarity;  // SPI_CPOL_0 = clock kept low when not in use, ___-_-
                                // SPI_CPOL_1 = clock kept high when not in use ---_-_
    spi_cpha_t clock_phase;  // SPI_CPHA_0 = capture on first clock transition, ---|_-_
                             // SPI_CPHA_1 = capture on second clock transition ---_|-_
    spi_order_t bits_order;  // SPI_LSP_FIRST, SPI_MSB_FIRST
};  // struct SpiFormat

class SpiInterface {
    struct Members {
        uint baudrate;
        spi_inst_t *spi;
        SpiPins pins;
        SpiFormat format;
        uint8_t dummy_value;
        bool has_inited;
    } m;

   public:
    SpiInterface(SpiPins pins, uint baud, spi_inst_t *spi, SpiFormat format, uint8_t dummy)
        : m(Members{ .baudrate = baud,
                     .spi = spi,
                     .pins = pins,
                     .format = format,
                     .dummy_value = dummy })
    {
    }
    virtual ~SpiInterface()
    {

        if(m.has_inited)
        {
            deinit();
        }
    }

    io::Status init()
    {
        io::Status status{ status_t::success, status_label };
        if(!m.has_inited)
        {
            m.has_inited = true;
            // setup CS pin
            gpio_init(m.pins.cs);
            gpio_set_dir(m.pins.cs, GPIO_OUT);
            chip_enable(false);
            // setup spi
            spi_init(m.spi, m.baudrate);
            spi_set_format(m.spi,
                           m.format.bits_count,
                           m.format.clock_polarity,
                           m.format.clock_phase,
                           m.format.bits_order);
            // setup spi pins
            gpio_set_function(m.pins.sck, GPIO_FUNC_SPI);
            gpio_set_function(m.pins.mosi, GPIO_FUNC_SPI);
            gpio_set_function(m.pins.miso, GPIO_FUNC_SPI);
            busy_wait_us(50);
        }
        else
        {
            status = status_t::err_already_init_fail;
        }
        return status;
    }

    io::Status deinit()
    {
        io::Status status{ status_t::success, status_label };
        if(m.has_inited)
        {
            spi_deinit(m.spi);
            gpio_deinit(m.pins.cs);
            m.has_inited = false;
        }
        else
        {
            LOG_E("err_init_fail");
            status = status_t::err_init_fail;
        }
        return status;
    }

    inline io::Status chip_enable(bool enable)
    {
        io::Status status{ status_t::success, status_label };
        if(m.has_inited)
        {
            gpio_put(m.pins.cs, !enable);
        }
        else
        {
            LOG_E("err_init_fail");
            status = status_t::err_init_fail;
        }
        return status;
    }

    inline io::Status write(const uint8_t *msg, const std::size_t len)
    {
        io::Status status{ status_t::success, status_label };
        if(m.has_inited)
        {
            std::size_t bytes_written = spi_write_blocking(m.spi, msg, len);
            if (bytes_written < len) {
                LOG_E("write incomplete %zu < %zu", bytes_written, len);
                status = status_t::err_write_incomplete;
            }
        }
        else
        {
            LOG_E("err_init_fail");
            status = status_t::err_init_fail;
        }
        return status;
    }

    inline io::Status read(uint8_t *buf, const std::size_t max_len, ssize_t &read_len)
    {
        io::Status status{ status_t::success, status_label };
        if(m.has_inited)
        {
            read_len = spi_read_blocking(m.spi, m.dummy_value, buf, max_len);
            if (read_len < static_cast<ssize_t>(max_len)) {
                LOG_E("read incomplete %zu < %zu", read_len, max_len);
                status = status_t::err_write_incomplete;
            }
        }
        else
        {
            LOG_E("err_init_fail");
            status = status_t::err_init_fail;
        }
        return status;
    }
};  // class isp
}  // namespace io
#endif  // define IO_SPI_H
