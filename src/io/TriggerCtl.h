#ifndef IO_TRIGGERCTL_H
#define IO_TRIGGERCTL_H
#include <cstdint>
#include "hardware/gpio.h"
#include "io.h"
#include "ncl/util/types.h"
#ifdef PICO_W
#include "net/wifi.h"
#endif //PICO_W

namespace io {

class TriggerCtl
{
    public:
        virtual io::Status set_on() = 0;
        virtual io::Status set_off() = 0;
        virtual io::Status invert() = 0;
        virtual bool is_on() = 0;
        virtual io::Status init() = 0;
        virtual io::Status deinit() = 0;
        virtual bool is_initialized() = 0;
};

class TriggerCtlGpio: public TriggerCtl
{
    protected:
    struct Members {
        io::pin_t pin;
        bool on;
        bool has_initialized;
    } m;

    public:
    TriggerCtlGpio(io::pin_t pin) : m( Members{ .pin = pin})
    {
    }

    ~TriggerCtlGpio()
    {
        if(is_initialized())
        {
            deinit();
        }
    }

    virtual bool is_initialized()
    {
        return m.has_initialized;
    }

    virtual bool is_on()
    {
        return m.on;
    }

    virtual io::Status init()
    {
        io::Status status{ status_t::success, status_label };
        if(m.pin < 32)
        {
            gpio_init(m.pin);
            gpio_set_dir(m.pin, GPIO_OUT);
            m.has_initialized = true;
        } else {
            status = io::status_t::err_init_fail;
            LOG_E("error: pin out of range");
        }
        return status;
    }

    io::Status deinit()
    {
        io::Status status{ status_t::success, status_label };
        if(is_initialized())
        {
            gpio_deinit(m.pin);
            m.has_initialized = false;
        } else {
            LOG_E("error: not yet initialized");
            status = io::status_t::err_not_init_fail;
        }
        return status;
    }

    virtual io::Status set_on()
    {
        io::Status status{ status_t::success, status_label };
        if(is_initialized())
        {
            m.on = 1;
            gpio_put(m.pin, m.on);
        } else {
            status = io::status_t::err_not_init_fail;
        }
        return status;
    }

    virtual io::Status set_off()
    {
        io::Status status{ status_t::success, status_label };
        if(is_initialized())
        {
            m.on = 0;
            gpio_put(m.pin, m.on);
        } else {
            status = io::status_t::err_not_init_fail;
        }
        return status;
    }

    virtual io::Status invert()
    {
        if(!is_on())
        {
            return set_on();
        }
        else
        {
            return set_off();
        }
    }
}; //class TriggerCtlGpio

} //namespace io

#endif //#define IO_TRIGGERCTL_H
