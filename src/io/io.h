#ifndef IO_IO_H
#define IO_IO_H

#include "ncl/util/types.h"
#include "ncl/util/Status.h"

namespace io {

enum status_t {
    okay_ = -127,
    success = 0,
    err_generic_fail,
    err_write_incomplete,
    err_already_init_fail,
    err_init_fail,
    err_not_init_fail,
};  // enum status_t

const std::map<status_t, std::string> status_label = {
    { success, "io success" },
    { err_generic_fail, "io generic error" },
    { err_write_incomplete, "io incomplete write error" },
    { err_already_init_fail, "io already initialized error" },
    { err_init_fail, "io not yet initialized error" },
};  // map<> status_label

using Status = util::Status<status_t>;

using pin_t = unsigned int;

}  // namespace io
#endif  // define IO_IO_H
