#ifndef IO_TRIGGERCTLW_H
#define IO_TRIGGERCTLW_H
#include "io/TriggerCtl.h"
#include "net/wifi.h"

namespace io {

class TriggerCtlW : public TriggerCtlGpio
{

    public:
    TriggerCtlW(io::pin_t pin) : TriggerCtlGpio(pin)
    {
    }

    virtual io::Status init()
    {
        io::Status status{ status_t::success, status_label };
        if(!is_initialized())
        {
            if(!net::WifiDriver::has_initialized())
            {
                net::WifiDriver::init();
            }
            m.has_initialized = true;
        } else {
            LOG_E("error: not yet initialized");
            status = io::status_t::err_not_init_fail;
        }

        return status;
    }

    virtual io::Status set_on()
    {
        io::Status status{ status_t::success, status_label };
        if(is_initialized())
        {
            m.on = 1;
            cyw43_arch_gpio_put(m.pin, m.on);
        } else {
            status = io::status_t::err_not_init_fail;
        }
        return status;
    }

    virtual io::Status set_off()
    {
        io::Status status{ status_t::success, status_label };
        if(is_initialized())
        {
            m.on = 0;
            cyw43_arch_gpio_put(m.pin, m.on);
        } else {
            status = io::status_t::err_not_init_fail;
        }
        return status;
    }
}; //class TriggerCtlW

} //namespace io

#endif //#define IO_TRIGGERCTLW_H
