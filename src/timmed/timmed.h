#ifndef TIMMED_TIMMED_H
#define TIMMED_TIMMED_H

#include "ncl/util/types.h"
#include "ncl/util/Status.h"

namespace timmed {

enum status_t {
    okay_ = -127,
    success = 0,
    err_generic_fail,
    err_already_init_fail,
    err_init_fail,
    err_not_init_fail,
    err_sm_already_started,
};  // enum status_t

const std::map<status_t, std::string> status_label = {
    { success, "timmer success" },
    { err_generic_fail, "timmer generic error" },
    { err_already_init_fail, "timmer already initialized error" },
    { err_init_fail, "timmer initialized error" },
    { err_not_init_fail, "timmer not yet initialized error" },
    { err_sm_already_started, "timmer already started error" },
};  // map<> status_label

using Status = util::Status<status_t>;

}

#endif // define TIMMED_TIMMED_H
