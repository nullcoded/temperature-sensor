#ifndef UTIL_TIMMEDTRIGGER_H
#define UTIL_TIMMEDTRIGGER_H
#include "io/TriggerCtl.h"
#include "timmed/timmed.h"
#include <string.h>

namespace timmed {

class TimmedTrigger {
    enum trigger_sm {
        wait_for_init,
        wait_for_start,
        wait_for_duration,
        reset,
        error,
    };
    const std::map<trigger_sm, std::string> sm_label = {
        { trigger_sm::wait_for_init, "wait_for_init" },
        { trigger_sm::wait_for_start, "wait_for_start" },
        { trigger_sm::wait_for_duration, "wait_for_duration" },
        { trigger_sm::reset, "reset" },
        { trigger_sm::error, "error" },
    };  // map<> status_label

    io::TriggerCtl &trigger;
    struct Members {
        trigger_sm sm;
        std::uint64_t next_start_ts;
        std::uint32_t duration_s;
        std::uint64_t duration_start_ts;
    } m;

    bool has_start_time_elapsed(std::uint64_t now_ts)
    {
        //LOG_D("now_ts: %u < next: %u", now_ts, m.next_start_ts, (now_ts < m.next_start_ts));
        return now_ts >= m.next_start_ts;
    }

    bool has_duration_elapsed(std::uint64_t now_ts)
    {
        //LOG_D("now_ts: %llu <= next: %llu, elapsed: %u", now_ts, (m.duration_start_ts + m.duration_s), (now_ts >= (m.duration_start_ts + m.duration_s)) );
        return now_ts >= (m.duration_start_ts + m.duration_s);
    }

    public: 
    TimmedTrigger(io::TriggerCtl &t, std::uint32_t duration)
        : trigger(t), m(Members{ .sm = trigger_sm::wait_for_init, .duration_s = duration })
    {
    }

    ~TimmedTrigger()
    {
        if(trigger.is_initialized() && trigger.is_on())
        {
            trigger.set_off();
        }
    }

    Status setNextStart(std::uint64_t next_start_ts)
    {
        timmed::Status status{ status_t::success, status_label };
        m.next_start_ts = next_start_ts;
        return status;
    }

    Status begin()
    {
        timmed::Status status{ status_t::success, status_label };
        if(!trigger.is_initialized())
        {
            if(trigger.init().is_error())
            {
                status = status_t::err_init_fail;
                LOG_E("error: unable to initialize trigger");
                return status;
            }
        }
        if(is_started())
        {
            status = status_t::err_sm_already_started;
        }
        else
        {
            set_sm(trigger_sm::wait_for_start);
        }
        return status;
    }

    bool is_started()
    {
        switch (m.sm) {
            case trigger_sm::wait_for_start: {
                return true;
            }
            case trigger_sm::wait_for_duration: {
                return true;
            }
            case trigger_sm::reset: {
                return true;
            }
            case trigger_sm::wait_for_init: {
                return false;
            }
            case trigger_sm::error: {
                return true;
            }
        }
        return false;
    }

    Status set_sm(trigger_sm next_sm)
    {
        timmed::Status status{ status_t::success, status_label };
        LOG_D("from '%s' to '%s'", sm_label.at(m.sm).c_str(), sm_label.at(next_sm).c_str());
        m.sm = next_sm;
        return status;
    }

    Status poll(uint64_t now_ts)
    {
        timmed::Status status{ status_t::success, status_label };
        //LOG_D("poll:NextStart: %llu", m.next_start_ts);
        //LOG_D("poll:sub: %lld", m.next_start_ts-now_ts);

        switch (m.sm) {
            case trigger_sm::wait_for_init: {
                if (is_started()) {
                    set_sm(trigger_sm::wait_for_start);
                }
                break;
            }
            case trigger_sm::wait_for_start: {
                if (has_start_time_elapsed(now_ts)) {
                    LOG_D("has_start_time_elapsed!");
                    m.duration_start_ts = now_ts;
                    m.next_start_ts += util::s_per_day;
                    trigger.set_on();
                    set_sm(trigger_sm::wait_for_duration);
                }
                break;
            }
            case trigger_sm::wait_for_duration: {
                if (has_duration_elapsed(now_ts)) {
                    trigger.set_off();
                    set_sm(trigger_sm::wait_for_start);
                }
                break;
            }
            case trigger_sm::reset: {
                set_sm(trigger_sm::wait_for_init);
                m.next_start_ts = 0;
                m.duration_start_ts = 0;
                trigger.set_off();
                break;
            }
            case trigger_sm::error: {
                break;
            }
        }
        return status;
    }
};

}  // namespace timmed

#endif  // UTIL_TIMMEDTRIGGER_H
