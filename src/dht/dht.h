#ifndef DHT_DHT_H
#define DHT_DHT_H
#include <memory>
#include <map>
#include <string>

#include "ncl/logging/logging.h"
#include "ncl/util/util.h"
#include "ncl/util/State.h"
#include "dht.pio.h"
#include "hardware/gpio.h"
#include "hardware/pio.h"
#include "pico/stdlib.h"

/**
 *  DHT11 - read daata from DHT11 humidity/temperature sensor.
 *  one wire communication with request, response.
 *  pullup resister attached to data pin.
 *
 *  Request: pull down for 18ms and release waiting for dht for 8ms then dht will pull down for 80us
 * then up for 80us then data transfer begins.
 *
 *  VCC          -8us-        --80us--
 *  GND--18ms..--     --80us--        _data begin_
 *
 *  Data: 0 bit: down for 50us up for 20 us (70us total) VCC -20us-
 *  GND--50us--      _next bit begin_
 *
 *  1 bit: down for 50us, up for 70 us (120us total)
 *  VCC        --70us--
 *  GND--50us--        _next bit begin_
 *
 *  0 bit: down for 50us, up for 20 us (70us total)
 *  VCC        --20us--
 *  GND--50us--        _next bit begin_
 *
 *  The data is composed of 40 bits, little endian.
 *  16bit float - Relative Humidity 20%-90%
 *  16bit float - Tempreature 0c-50c
 *  8bit int - sum(data[0:32])==data[33:39]
 */

namespace dht11 {

const uint wake_pulse_duration_ms = 18;
const uint64_t refresh_rate_us = 2*util::us_per_s;
const uint data_byte_size = 5;
const uint invalid_pin = -1;
const uint64_t request_delay_us = 25*util::us_per_ms;

enum implementation_t { bitbang, pio };

struct Reading {
    uint8_t relative_humidity;
    uint8_t relative_humidity_decimal;
    uint8_t temperature;
    uint8_t temperature_decimal;
    uint8_t checksum;
    uint64_t timestamp;
};

union ReadingUnion {
    uint8_t bytes[data_byte_size];
    Reading ints;
    uint32_t u32;
};

class SensorCallback
{
    public:
    virtual void operator()(Reading reading)=0;
};

class Sensor;
class SensorBitbang;
class SensorPio;

void print_reading(const Reading &reading);
bool reading_is_valid(const Reading &reading);
std::unique_ptr<Sensor> init_sensor(uint pin,
                                    bool debug_pin_enable,
                                    uint debug_pin,
                                    implementation_t implementation);

class Sensor {
    struct Members {
        Reading last_reading;
        uint pin;
        uint debug_pin;
        bool debug_pin_is_enabled;
    } m;
    inline Sensor(Members &&m_) : m(m_)
    {
    }

   public:
    Sensor(uint pin, bool debug_pin_enable, uint debug_pin)
        : Sensor(
          Members{ .pin = pin, .debug_pin = debug_pin, .debug_pin_is_enabled = debug_pin_enable })
    {
    }
    virtual ~Sensor()
    {
        m={};
    }
    Sensor(Sensor&& other)
        : m(std::move(other.m))
    {
        other.m={.pin=invalid_pin, .debug_pin=invalid_pin};
    }
    Sensor &operator=(Sensor &&other) {
        m = std::move(other.m);
        return *this;
    }

    //disallow copies
    Sensor(const Sensor&) = delete;
    Sensor& operator=(const Sensor&) = delete;

    uint get_pin();
    uint get_debug_pin();
    bool get_debug_enabled();
    Reading get_last_reading();
    void set_last_reading(const Reading new_reading);
    virtual void send_request() = 0;
    virtual Reading read_data() = 0;
    virtual void poll() {};
    virtual void set_callback(SensorCallback *p_callback) {};
    
};  // class Sensor;

class SensorBitbang : public Sensor {
    struct Members {
    } m;
    uint8_t read_bit();

   public:
    SensorBitbang(uint pin_num, bool debug_pin_enabled, uint debug_pin_num)
        : Sensor(pin_num, debug_pin_enabled, debug_pin_num)
    {
        gpio_init(get_pin());
        LOG_D("debug_enabled: %s", get_debug_enabled() ? "true" : "false");
        LOG_D("debug_pin: %d", get_debug_pin());
        if (get_debug_enabled()) {
            gpio_init(get_debug_pin());
            gpio_set_dir(get_debug_pin(), GPIO_OUT);
            gpio_put(get_debug_pin(), 0);
        }
    }
    virtual ~SensorBitbang()
    {
    }
    virtual void send_request();
    virtual Reading read_data();
    virtual Reading measure();
};  // class SensorBitbang

class SensorPio : public Sensor {
    enum state_t {
        state_init,
        state_idle,
        state_wait,
        state_data_pending,
    };

    const std::map<state_t, std::string> state_labels = {
        {state_init, "state_init"},
        {state_idle, "state_idle"},
        {state_wait, "state_wait"},
        {state_data_pending, "state_data_pending"},
    };

    void init_pio();

    struct Members {
        uint sm;
        PIO pio;
        uint offset;
        uint data_count;
        ReadingUnion u_intermediate_reading;
        SensorCallback * p_callback;
    } m;
    util::State<state_t> state;
    
    inline SensorPio(uint pin_num, bool debug_pin_enabled, uint debug_pin_num, Members &&m_)
        : Sensor(pin_num, debug_pin_enabled, debug_pin_num), m(m_), state(state_init, state_labels)
    {
    }

   public:
    SensorPio(uint pin_num, bool debug_pin_enabled, uint debug_pin_num)
        : SensorPio(pin_num, debug_pin_enabled, debug_pin_num, Members{ .pio=pio0, .p_callback=nullptr})
    {
        init_pio();
        state.set(state_idle);
        //state.enable_logging(true);
    }
    virtual ~SensorPio()
    {
        m=Members{};
    }
    virtual void send_request();
    virtual Reading read_data();
    virtual void poll();
    void set_callback(SensorCallback *p_callback);
};  // class SensorPio

}  // namespace dht11
#endif  // DHT_DHT_H
