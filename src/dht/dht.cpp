#include "dht/dht.h"

#include "hardware/clocks.h"
#include "ncl/util/util.h"

/* class Sensor functions */
uint dht11::Sensor::get_pin()
{
    return m.pin;
}

uint dht11::Sensor::get_debug_pin()
{
    return m.debug_pin;
}

bool dht11::Sensor::get_debug_enabled()
{
    return m.debug_pin_is_enabled;
}

dht11::Reading dht11::Sensor::get_last_reading()
{
    return m.last_reading;
}

void dht11::Sensor::set_last_reading(const dht11::Reading new_reading)
{
    m.last_reading = new_reading;
}

/* class SensorBitbang functions */
dht11::Reading dht11::SensorBitbang::measure()
{
    // LOG_D("measure - pin:%d", pin);
    dht11::Reading reading = read_data();
    if (reading_is_valid(reading)) {
        set_last_reading(reading);
    } else {
        LOG_E("%s", "Reading failed checksum.");
    }
    return reading;
}

uint8_t dht11::SensorBitbang::read_bit()
{
    const uint max_bit_low_time_us = 50;
    const uint max_bit_high_time_us = 70;
    const uint zero_bit_time_us = 20;
    const uint pin = get_pin();
    uint count = 0;
    for (uint t = 0; t < max_bit_low_time_us; ++t) {
        if (gpio_get(pin) == 1) {
            ++count;
            if (get_debug_enabled()) {
                gpio_put(get_debug_pin(), 1);
            }
            break;
        }
        sleep_us(1);
    }
    if (count != 1) {
        LOG_E("%s", "Reading bit failed!");
    }
    for (uint t = 0; t < max_bit_high_time_us; ++t) {
        uint current = gpio_get(pin);
        if (1 == current) {
            ++count;
        } else {
            if (get_debug_enabled()) {
                gpio_put(get_debug_pin(), 0);
            }
            break;
        }
        sleep_us(1);
    }
    return count > zero_bit_time_us ? 1 : 0;
}
void dht11::SensorBitbang::send_request()
{
    // add 10us of padding to prevent premature bit1 count
    const uint wait_for_data_us = 180 + 10;
    const uint pin = this->get_pin();
    gpio_put(pin, 0);
    gpio_set_dir(pin, GPIO_OUT);
    gpio_put(pin, 0);
    sleep_ms(wake_pulse_duration_ms);  // let DHT11 detect signal
    gpio_put(pin, 1);
    if (get_debug_enabled()) {
        gpio_put(get_debug_pin(), 1);
    }
    gpio_set_dir(pin, GPIO_IN);
    sleep_us(wait_for_data_us);  // wait for DHT11 response, from 20 to 40 ms
    if (get_debug_enabled()) {
        gpio_put(get_debug_pin(), 0);
    }
}
dht11::Reading dht11::SensorBitbang::read_data()
{
    ReadingUnion measurement = {};
    const uint bits_to_read = dht11::data_byte_size * 8;
    uint8_t *bytes = measurement.bytes;
    send_request();
    for (uint i = 0; i < bits_to_read; ++i) {
        if (this->read_bit()) {
            bytes[i / 8] |= (1 << (7 - (i % 8)));
        }
    }
    measurement.ints.timestamp = time_us_64();
    print_reading(measurement.ints);
    return measurement.ints;
}

/* class SensorPio functions */

void dht11::SensorPio::init_pio()
{
    const uint pio_freq = 1000000;
    const uint pin = this->get_pin();
    m.offset = pio_add_program(m.pio, &dht_program);
    m.sm = pio_claim_unused_sm(m.pio, true);
    pio_sm_config sm_conf = dht_program_get_default_config(m.offset);
    float div = ((float)clock_get_hz(clk_sys)) / pio_freq;
    // LOG_I("div:%f", div);
    sm_config_set_clkdiv(&sm_conf, div);
    sm_config_set_out_pins(&sm_conf, pin, 1);
    sm_config_set_in_pins(&sm_conf, pin);
    sm_config_set_jmp_pin(&sm_conf, pin);
    sm_config_set_set_pins(&sm_conf, pin, 1);
    sm_config_set_in_shift(&sm_conf, false, true, 8);
    pio_gpio_init(m.pio, pin);
    if (get_debug_enabled()) {
        const uint side_set_pin = this->get_debug_pin();
        sm_config_set_sideset_pins(&sm_conf, side_set_pin);
        pio_gpio_init(m.pio, side_set_pin);
        pio_sm_set_consecutive_pindirs(m.pio, m.sm, side_set_pin, 1, true);
    }
    pio_sm_set_consecutive_pindirs(m.pio, m.sm, pin, 1, true);
    pio_sm_init(m.pio, m.sm, m.offset, &sm_conf);
}

void dht11::SensorPio::send_request()
{
    pio_sm_restart(m.pio, m.sm);
    pio_sm_clear_fifos(m.pio, m.sm);
    pio_sm_clkdiv_restart(m.pio, m.sm);
    pio_sm_exec(m.pio, m.sm, pio_encode_jmp(m.offset));
    pio_sm_set_enabled(m.pio, m.sm, true);
    const uint32_t wait_pulse_us = dht11::wake_pulse_duration_ms * util::us_per_ms;
    // LOG_D("put: %d", wait_pulse_us);
    pio_sm_put(m.pio, m.sm, wait_pulse_us);
    m.data_count = 0;
    m.u_intermediate_reading = {};
    state.set(state_wait);
}

dht11::Reading dht11::SensorPio::read_data()
{
    uint level = pio_sm_get_rx_fifo_level(m.pio, m.sm);
    uint8_t *p_data = m.u_intermediate_reading.bytes;
    while (level > 0 && m.data_count < data_byte_size) {
        // LOG_D("level: %d", level);
        p_data[m.data_count] = pio_sm_get(m.pio, m.sm);
        level = pio_sm_get_rx_fifo_level(m.pio, m.sm);
        m.data_count += 1;
    }
    return m.u_intermediate_reading.ints;
}

void dht11::SensorPio::poll()
{
    if (state.has_time_elapsed_us(refresh_rate_us * 2)) {
        state.set(state_idle);
    }
    switch (state.get()) {
    case state_idle: {
        if (state.has_time_elapsed_us(refresh_rate_us)) {
            send_request();
        }
        break;
    }
    case state_wait: {
        if (state.has_time_elapsed_us(request_delay_us)) {
            state.set(state_data_pending);
        }
        break;
    }
    case state_data_pending: {
        Reading new_reading = read_data();
        if (m.data_count >= data_byte_size) {
            new_reading.timestamp = time_us_64();
            print_reading(new_reading);
            state.set(state_idle);
            if (reading_is_valid(new_reading)) {
                set_last_reading(new_reading);
                if (m.p_callback) {
                    (*m.p_callback)(new_reading);
                }
            }
        } else if (state.has_time_elapsed_us(request_delay_us)) {
            state.set(state_idle);
        }
        if (state.get() == state_idle) {
            //pio_sm_set_enabled(m.pio, m.sm, false);
        }
        break;
    }
    default:
        // do nothing;
        return;
    }
}

void dht11::SensorPio::set_callback(dht11::SensorCallback *p_callback)
{
    m.p_callback = p_callback;
}

/* helper functions */

void dht11::print_reading(const dht11::Reading &reading)
{
    bool csum_pass = reading_is_valid(reading);
#if DHT11
    LOG("relative_humidity: %d, temperature: %d, csum: %s",
        reading.relative_humidity,
        reading.temperature,
        csum_pass ? "pass" : "fail");
#else
    double humidity = ((reading.relative_humidity << 8) | reading.relative_humidity_decimal) / 10.0;
    double temperature = (reading.temperature >> 7 ? -1 : 1)
                         * (((reading.temperature << 8) | reading.temperature_decimal) / 10.0);
    LOG("relative_humidity: %f, %d, temperature: %f, %d, csum: %s",
        humidity,
        reading.relative_humidity,
        temperature,
        reading.temperature,
        csum_pass ? "pass" : "fail");
#endif
}

bool dht11::reading_is_valid(const dht11::Reading &reading)
{
    uint csum = ((reading.relative_humidity + reading.relative_humidity_decimal
                  + reading.temperature + reading.temperature_decimal)
                 % 256);
    int csum_delta = (csum % 256) - reading.checksum;
    LOG_D("csum delta: %u-%u=%d", (csum % 256), reading.checksum, csum_delta);
    //TODO: fix chesum error when  last checksum bit is 0;
    //HACK: bind csum_delta to a difference of 1
    return (csum_delta > -2) && (csum_delta < 1);
}

std::unique_ptr<dht11::Sensor> dht11::init_sensor(const uint pin,
                                                  const bool debug_pin_enable,
                                                  const uint debug_pin,
                                                  const dht11::implementation_t implementation)
{
    switch (implementation) {
    case dht11::implementation_t::pio:
        return std::make_unique<dht11::SensorPio>(pin, debug_pin_enable, debug_pin);
    case dht11::implementation_t::bitbang:
    default: {
        return std::make_unique<dht11::SensorBitbang>(pin, debug_pin_enable, debug_pin);
    }
    }
}
