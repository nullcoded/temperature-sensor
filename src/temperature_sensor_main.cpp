/**
 *	Temperature Sensor - read temperaure from DHT-11 module and log to uart0
 */

#include <map>
#include <memory>
#include <string>

#include "FreeRTOS.h"
#include "dht/dht.h"
#include "io/spi.h"
#include "ncl/logging/logging.h"
#include "net/tcp_client.h"
#include "ncl/buffers/ArraySlice.h"
#include "net/wifi.h"
#include "pico/platform.h"
#include "pico/stdlib.h"
#include "secrets.h"
#include "task.h"
#include "io/TriggerCtl.h"
#include "io/TriggerCtlW.h"
#include "timmed/TimmedTrigger.h"
#include "memory/flash.h"

#ifndef SSID
# error missing SSID define
#endif  // ifndef SSID
#ifndef WIFI_PW
# error missing WIFI_PW define
#endif  // ifndef WIFI_PW

#ifdef PICO_DEFAULT_LED_PIN
# define LED_PIN PICO_DEFAULT_LED_PIN
#endif  // ifdef PICO_DEFAULT_LED_PIN

#define DEBUG_PIN 2
#define DHT11_PIN 3

namespace senack {
enum state_t { idle_state, establish_protocol, register_device, report_data };
std::map<state_t, std::string> state_labels = {
    { idle_state, "idle_state" },
    { establish_protocol, "establish_protocol" },
    { register_device, "register_device" },
    { report_data, "report_data" },
};

class App {
   public:
    class TemperatureCallback : public dht11::SensorCallback {
       public:
        struct Members {
            uint32_t reading_count;
        } m;
        App &app;
        TemperatureCallback(App *a) : m(Members{}), app(*a)
        {
        }
        virtual void operator()(dht11::Reading reading)
        {
            LOG_D("state:%s", app.state.as_string().c_str());
            if (app.state == establish_protocol) {
                m.reading_count += 1;
                buffers::ArrayBuffer &out_buffer = app.tcp_client.get_write_buffer();
                // double humidity = ((reading.relative_humidity << 8) |
                // reading.relative_humidity_decimal) / 10.0;
                double temperature
                  = (reading.temperature >> 7 ? -1 : 1)
                    * (((reading.temperature << 8) | reading.temperature_decimal) / 10.0);
                app.parser.write_temperature_message(out_buffer, temperature, reading.timestamp);
                // app.parser.writeHumidityMessage(out_buffer);
                app.tcp_client.finalize_write_buffer();
                LOG_D("out: %u", out_buffer.len());
            }
        }
    } temp_cb;

    class OnTcpEventCallback : public net::OnTcpEventCallback {
       public:
        struct Members {
        } m;
        App &app;
        OnTcpEventCallback(App *a) : m(Members{}), app(*a)
        {
        }
        virtual void operator()(net::tcp_event_t event)
        {
            switch (event) {
            case net::connect_event: {
                buffers::ArrayBuffer &out_buffer = app.tcp_client.get_write_buffer();
                app.parser.write_start_message(out_buffer);
                app.tcp_client.finalize_write_buffer();
                app.state = establish_protocol;
                break;
            }
            case net::receive_data_event: {
                break;
            }
            default: {
                LOG_E("received unhandleded event %s", net::tcp_event_labels.at(event).c_str());
            }
            }
        }
    } on_tcp_event_cb;
    util::State<senack::state_t> state;
    net::Wifi wifi;
    senack::Parser parser;
    net::TcpClient tcp_client;
    io::TriggerCtlW led;
    std::unique_ptr<dht11::Sensor> sensor;

    App()
        : temp_cb(this),
          on_tcp_event_cb(this),
          state(senack::state_t::idle_state, senack::state_labels),
          wifi(SSID, WIFI_PW, CYW43_AUTH_WPA2_AES_PSK, 30000),
          parser(),
          tcp_client(HOST_IP_STR, HOST_PORT),
          led(CYW43_WL_GPIO_LED_PIN),
          sensor(init_sensor(DHT11_PIN, true, DEBUG_PIN, dht11::implementation_t::pio))
    {
        tcp_client.set_event_callback(&on_tcp_event_cb);
        wifi.set_callback(tcp_client.get_net_callback());
        sensor.get()->set_callback(&temp_cb);
        state.enable_logging(true);
    }

    void run()
    {
        wifi.connect();
        uint64_t t = time_us_64();
        while (true) {
            wifi.poll();
            tcp_client.poll();
            sensor.get()->poll();
            if (time_us_64() - t > util::us_per_s) {
                // LOG("%s", "poll");
                t = time_us_64();
            }
        }
    }
};  // class App
}  // namespace senack

void app_run(void *p_none)
{
    LOG_D("init wifi driver core %d", get_core_num());
    net::WifiDriver::init();
    LOG_D("%s", " init app");
    senack::App app{};
    LOG_D("%Rs", "app::run");
    app.run();
    vTaskDelete(NULL);
}

#define SPI_FLASH_DEFFERED_ISR_TEST 0
#if SPI_FLASH_DEFFERED_ISR_TEST
std::int64_t spi_flash_isr_callback(alarm_id_t id, void *context)
{
    LOG_D("spi_flash_isr_callback id: %u", id);
    if(nullptr == context)
    {
        LOG_E("expected a handle got null: %p", context);
        return 0;
    }
    TaskHandle_t handle = *reinterpret_cast<TaskHandle_t *>(context);
    LOG_D("%s", "go back!");
    BaseType_t xYieldRequired = xTaskResumeFromISR(handle);
    portYIELD_FROM_ISR(xYieldRequired);
    LOG_D("%s", "done!");
    return 0;
}

//Deffered Interrupt example
void spi_flash_deffered_isr_test(void *p_none)
{
    memory::Flash storage{};
    TaskHandle_t handle = xTaskGetHandle("App");
    memory::Status status = storage.init();
    buffers::ArrayBuffer buf_data{40};
    std::uint8_t data[] = {42, 1,2,3,4,5,6,7,8,9,10};
    std::uint32_t address = 0x00;
    std::size_t data_len = sizeof(data);
    std::size_t read_count = 0;
    std::size_t write_count = 0;
    std::uint8_t stat = 0;
    storage.set_status(1, 0, 0, 0, 0, 0);
    //begin aai
    storage.enable_write();
    std::size_t temp_write_count = 0;
    status = storage.set_aai(address, data+write_count, data_len-write_count, temp_write_count);
    write_count += temp_write_count;
    while(status.is_success())
    {
        bool is_busy = true;
        uint count = 0;
        while(status.is_success() && is_busy)
        {
            std::uint8_t stat = 0;
            add_alarm_in_us(500, spi_flash_isr_callback, reinterpret_cast<void*>(&handle), false);
            vTaskSuspend(handle);
            status = storage.read_status(stat);
            is_busy = stat & memory::status_reg_mask::busy_mask;
            ++count;
        }
        if(status.is_success() && (data_len-write_count) > 0)
        {
            status = storage.aai_write(data+write_count, data_len-write_count, temp_write_count);
            write_count += temp_write_count;
        }
        else
        {
            break;
        }
    }
    
    LOG_D("%s", "\n\n!!read_data\n");
    storage.read_data(buf_data.data(), buf_data.capacity(), 0x00, read_count);
    buf_data.len(read_count);
    buf_data.print_as_hex();
    vTaskDelete(NULL);
}
#endif // if SPI_FLASH_DEFFERED_ISR_TEST

#define SPI_FLASH_BLOCKING_WRITE_TEST 0
#if SPI_FLASH_BLOCKING_WRITE_TEST
void spi_flash_blocking_write_test(void *p_none)
{
    LOG_D("init spi: %d", get_core_num());
    memory::Flash storage{};
    storage.init();
    if(true) {
        buffers::ArrayBuffer buf_data{20};
        std::uint8_t data[] = {1,2,3,4,5,6,7,8,9,10};
        std::size_t data_len = sizeof(data);
        std::size_t read_count = 0;
        std::uint8_t status = 0;

        storage.read_jedec(buf_data.data(), buf_data.capacity(), read_count);
        buf_data.len(read_count);
        LOG_D("\n\n!!read_jedec: %d\n", read_count);
        buf_data.print_as_hex();

        LOG_D("%s", "\n\n!!set_status\n");
        storage.set_status(1, 0, 0, 0, 0, 0);

        if(1) {
            LOG_D("%s", "\n\n!!enable write\n");
            //storage.set_byte_blocking(0x01, 0x42);
            std::size_t write_count = 0;
            storage.blocking_write(0x00, data, data_len, write_count);
        }

        LOG_D("%s", "\n\n!!read_data\n");
        storage.read_data(buf_data.data(), buf_data.capacity(), 0x00, read_count);
        buf_data.len(read_count);
        buf_data.print_as_hex();
    }
    vTaskDelete(NULL);
}
#endif //if SPI_FLASH_BLOCKING_WRITE_TEST

#define SPI_FLASH_BLOCKING_ERASE_TEST 1
#if SPI_FLASH_BLOCKING_ERASE_TEST
void test_flash_erase_4k_run(void *p_none)
{
    LOG_D("init spi: %d", get_core_num());
    memory::Flash storage{};
    storage.init();
    if(true) {


        LOG_D("%s", "\n\n!!read_status\n");
        std::uint8_t status = 0;
        storage.read_status(status);
        storage.print_status(status);

        if(1) {
            LOG_D("%s", "\n\n!!set_status\n");
            storage.set_status(1, 0, 0, 0, 0, 0);
            LOG_D("%s", "\n\n!!blocking_erase\n");
            storage.blocking_erase_4k(0x00);
        }
        LOG_D("%s", "\n\n!!read_data\n");
        buffers::ArrayBuffer buf_data{20};
        ssize_t read_count = 0;
        storage.read_data(buf_data.data(), buf_data.capacity(), 0x00, read_count);
        buf_data.len(read_count);
        buf_data.print_as_hex();
    }
    vTaskDelete(NULL);
}
#endif //if SPI_FLASH_BLOCKING_ERASE_TEST

#if configCHECK_FOR_STACK_OVERFLOW
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName)
{
    LOG_D("overflow!: %s", pcTaskName);
    while (1) {
    };
}
#endif  // if configCHECK_FOR_STACK_OVERFLOW

int main()
{
    stdio_init_all();
    util::init(&time_us_64);
    logging::init(logging::log_level_t::verbose);

    TaskHandle_t app_task_h;
#if SPI_FLASH_DEFFERED_ISR_TEST
    BaseType_t task_status = xTaskCreate(&spi_flash_deffered_isr_test, "App", 2048, NULL, 0, &app_task_h);
#endif //if SPI_FLASH_DEFFERED_ISR_TEST
#if SPI_FLASH_BLOCKING_WRITE_TEST
    BaseType_t task_status = xTaskCreate(&spi_flash_blocking_write_test, "App", 2048, NULL, 0, &app_task_h);
#endif //if SPI_FLASH_BLOCKING_WRITE_TEST
#if SPI_FLASH_BLOCKING_ERASE_TEST
    BaseType_t task_status = xTaskCreate(&test_flash_erase_4k_run, "App", 2048, NULL, 0, &app_task_h);
#endif //if SPI_FLASH_BLOCKING_ERASE_TEST

    //BaseType_t task_status = xTaskCreate(&app_run, "App", 2048, NULL, 0, &app_task_h);
    vTaskCoreAffinitySet(app_task_h, 2);
    LOG_D("task created %d == %d", task_status, pdPASS);
    if (pdPASS == task_status) {
        vTaskStartScheduler();
    }
    LOG_D("%s", "scheduler exited!");
    return 0;
}
