# Pico Gardening

### Raspberry Pico W - gardening sensors and watering control

Uses a Raspberry-Pico (rp2040) coonected a temperature sensor module that reports back to senack server, a flash chip to hold settings and a relay to trigger a solenoid that controls a set of sprinklers.


## Temperature Sensor
![diagram](./pictures/temperature-sensor.png "temperature sensor pinout")
DHT11 - The module requires:
* 3 to 5 V on Vcc
* the data pin needs to have a pull up resistor connected to 3V3
The data pin will be connected to GP03 on the RPi Pico.
The RPi Pico will output a pulse on GP02 when doing a read, this is for debugging and can be disabled by setting `enable_debug_pin` to `false`.


![request](./pictures/dht11_request.png "request data")
To initiate a data request pull down the data pin for 18ms  and release for handover (set pin direction to in). The line will go up for 8us before being brought low by the DHT11 module for 80us then back high for 80us, after that the data transfer begins.

![request](./pictures/dht11_bits.png "bits")
A `1` bit is a total of 120us. The line is brought down for 50us then held high for 70us.
A `0` bit is a total of 70us. The line is brought down for 50us then held high for 20us.

![diagram](./pictures/dht11_capture.png "capture form scope")
The following was captures on a FNIRSI-101D and shows the handover and initial bits.

To begin initialize cmaken
> `$ ./scripts/init_build.sh`
> `$ cmake --build debug/ -j32`
> `$ cmake --install debug/`

clean build with
> `$ cmake --build debug --target clean`

[![qrcode](./pictures/nullcoded_qr.png "home page")](https://nullcoded.net)

## SPI Flash SST25VF080B - storage for settings
The flash chip has 8 megabits of storage and
The data sheet is abailable at ./docs/20005045C.pdf

* Capture of data should be performed on the rising edge.
* Commands are 8 bits and data is read sequentially until chip select is inactive.
* default erased value is `FF` with 4k erase sectors.

![diagram](./pictures/flash-sst25vf080b.png "flash sst25vf080b pinout")

### The Pins:

`#CE` chip enable, active low

`SO` signal out

`#WP` write protect, active low

`Vss` ground

`Vdd` power 5v

`HOLD` hold chip in inactive state

`SCK` clock

`SI` signal in


