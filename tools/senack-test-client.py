#!/usr/bin/env python3

import socket
import sys
import zlib


def crc32(data: bytes) -> int:
    return zlib.crc32(data) % (1 << 32)


def set_keepalive_linux(sock, after_idle_sec=1, interval_sec=1, max_fails=1):
    """Set TCP keepalive on an open socket.

    It activates after 1 second (after_idle_sec) of idleness,
    then sends a keepalive ping once every 3 seconds (interval_sec),
    and closes the connection after 5 failed ping (max_fails), or 15 seconds
    """
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, after_idle_sec)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, interval_sec)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, max_fails)


def serializeProtocolStart(protId: int, versions: list[int]):
    buf = [protId.to_bytes(4, 'little')]
    buf += [v.to_bytes(1, 'little') for v in versions]
    crc32_ = crc32(b''.join(buf))
    print("crc:", hex(crc32_), "data:", [hex(i) for i in b''.join(buf)])
    buf += [crc32_.to_bytes(4, 'little')]
    return b"".join(buf)


def serializeFloatData(msgId: int, seq: int, data: int, ts: int):
    size: int = 20  # data, ts, crc32
    # header size = 14, msgId, seq, size
    buf = [msgId.to_bytes(8, 'little')]
    buf += [seq.to_bytes(4, 'little')]
    buf += [size.to_bytes(2, 'little')]
    buf += [data.to_bytes(8, 'little')]
    buf += [ts.to_bytes(8, 'little')]
    crc32_ = crc32(b''.join(buf))
    print("crc:", hex(crc32_), "data:", [hex(i) for i in b''.join(buf)])
    buf += [crc32_.to_bytes(4, 'little')]
    return b"".join(buf)


def main() -> None:
    HOST: str = "127.0.0.1"
    PORT: int = 9001
    if len(sys.argv) == 3:
        HOST = sys.argv[1]
        PORT = int(sys.argv[2])

    print(HOST, PORT)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        set_keepalive_linux(conn)
        conn.connect((HOST, PORT))
        conn.sendall(serializeProtocolStart(1, [2, 3, 4, 5]))
        conn.sendall(serializeFloatData(7, 8, 9, 10))
        conn.sendall(serializeFloatData(11, 12, 13, 14))
        conn.sendall(serializeFloatData(15, 16, 17, 18))
        conn.sendall(serializeFloatData(19, 20, 21, 22))


if __name__ == "__main__":
    main()
