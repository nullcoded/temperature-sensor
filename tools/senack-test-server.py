#!/usr/bin/env python3
import socket
import sys

def set_keepalive_linux(sock, after_idle_sec=1, interval_sec=1, max_fails=1):
    """Set TCP keepalive on an open socket.

    It activates after 1 second (after_idle_sec) of idleness,
    then sends a keepalive ping once every 3 seconds (interval_sec),
    and closes the connection after 5 failed ping (max_fails), or 15 seconds
    """
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, after_idle_sec)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, interval_sec)
    sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, max_fails)

def parseProtocolStart(data : bytes) -> bytes:
    protId: int = 0
    versions:list[int] = []
    crc32: int = 0
    pos: int = 0
    if len(data) > pos+4:
        print(str(data[pos:pos+4]))
        protId = int.from_bytes(data[pos:pos+4], "little")
        print("protId:", hex(protId))
        pos+=4
    if len(data) > pos+4:
        print(str(data[pos:pos+4]))
        versions = list(data[pos:pos+4])
        print("versions:", versions)
        pos+=4
    if len(data) >= pos+4:
        print(str(data[pos:pos+4]))
        crc32 = int.from_bytes(data[pos:pos+4], "little")
        print("crc:", hex(crc32))
        pos+=4
    print("leftover data:", len(data[pos:]), "\n\n")
    return data[pos:]

def parseFloadDataMessage(data : bytes) -> bytes:
    messageId: int = 0
    seq: int = 0
    payload_len: int = 0
    value: float = 0
    ts: int = 0
    crc32: int = 0 
    pos: int = 0
    print("len:", len(data))
    if len(data) > pos+8:
        print(str(data[pos:pos+8]))
        messageId = int.from_bytes(data[pos:pos+8], "little")
        print("messageId:", messageId)
        pos+=8
    if len(data) > pos+4:
        print(str(data[pos:pos+4]))
        seq = int.from_bytes(data[pos:pos+4], "little")
        print("seq:", seq)
        pos+=4
    if len(data) > pos+2:
        print(str(data[pos:pos+2]))
        payload_len = int.from_bytes(data[pos:pos+2], "little")
        print("len:", payload_len)
        pos+=2
    if len(data) > pos+8:
        print(str(data[pos:pos+8]))
        #value = float(data[pos:pos+8])
        print("value:", [hex(i) for i in data[pos:pos+8]])
        pos+=8
    if len(data) > pos+8:
        print(str(data[pos:pos+8]))
        ts = int.from_bytes(data[pos:pos+8], "little")
        print("ts:", ts)
        pos+=8
    if len(data) >= pos+4:
        print(str(data[pos:pos+4]))
        crc32 = int.from_bytes(data[pos:pos+4], "little")
        print("crc32:", crc32)
        pos+=4
    print("pos:", pos, "leftover data:", len(data[pos:]),"\n\n")
    return data[pos:]

def main() -> None:
    HOST: str = "127.0.0.1"
    PORT:int = 9001
    if len(sys.argv) == 3:
        HOST=sys.argv[1]
        PORT=int(sys.argv[2])

    print(HOST, PORT)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, PORT))
        s.listen()
        while True:
            conn, addr = s.accept()
            set_keepalive_linux(conn)
            try:
                with conn:
                    print(f"Connected by {addr}")
                    data = conn.recv(12)
                    print("len:", len(data), str(data))
                    if not data:
                        break
                    parseProtocolStart(data)
                    while True:
                        data = conn.recv(34)
                        if not data:
                            print("brak1");
                            break
                        parseFloadDataMessage(data)
                    if not data:
                        print("brak2");
                        break
            except socket.error as e:
                print(e)

if __name__ == "__main__":
    main()
