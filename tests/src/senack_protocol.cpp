#include <string>
#include <utility>

#include "ncl/buffers/ArraySlice.h"
#include "ncl/logging/logging.h"
#include "ncl/util/Status.h"
#include "ncl/util/misc.h"
#include "ncl/util/types.h"
#include "protocol/Protocol.h"
#include "senack/Protocol.h"

bool test_init_protocol_message()
{
    senack::Protocol prot{};
    protocol::Message m;
    buffers::ArrayBuffer buf{ 34 };
    uint8_t sample_message[] = {
        0x04, 0x03, 0x02, 0x01, 0x05, 0x06, 0x07, 0x08, 0x00, 0x00, 0x00, 0x00,
    };
    size_t sample_message_len = sizeof(sample_message);
    protocol::Version version{ .major = 0x05, .minor = 0x06, .patch = 0x07, .build = 0x08 };
    auto status = protocol::build_start_message(0x01020304, version, m);
    status = m.serialize(buf);
    buf.print_as_hex();
    util::print_array(sample_message, sample_message_len);
    return (compare_array(sample_message,
                          sample_message_len - sizeof(protocol::crc32_t),
                          reinterpret_cast<uint8_t *>(buf.data()),
                          buf.len() - sizeof(protocol::crc32_t)));
}

bool test_temp_message()
{
    senack::Protocol prot{};
    senack::Message m;
    buffers::ArrayBuffer buf{ 34 };
    uint8_t sample_message[] = {
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /*msg_id*/
        0x01, 0x00, 0x00, 0x00,                         /*seq*/
        0x10, 0x00,                                     /*len*/
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x40, /*data*/
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /*ts*/
        0x00, 0x00, 0x00, 0x00,                         /*crc*/
    };
    size_t sample_message_len = sizeof(sample_message);
    auto status = prot.create_data_temperature_double(&m);
    status = prot.update_data_temperature_double(10.0, 0xffffffffffffffff, &m);
    status = m.serialize(buf);
    buf.print_as_hex();
    util::print_array(sample_message, sample_message_len);
    return (util::compare_array(sample_message,
                          sample_message_len - sizeof(protocol::crc32_t),
                          reinterpret_cast<uint8_t *>(buf.data()),
                          buf.len() - sizeof(protocol::crc32_t)));
}

/**
 * Add a message to the buffer then remove it and add another one.
 */
bool test_temp_messageReplace()
{
    senack::Protocol prot{};
    senack::Message m;
    buffers::ArrayBuffer buf{ 34 };
    uint8_t sample_message[] = {
        0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /*msg_id - 0*/
        0x01, 0x00, 0x00, 0x00,                         /*seq - 8*/
        0x10, 0x00,                                     /*len - 12*/
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x40, /*data - 14*/
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /*ts - 22*/
        0x00, 0x00, 0x00, 0x00,                         /*crc - 30*/
    };
    size_t sample_message_len = sizeof(sample_message);
    auto status = prot.create_data_temperature_double(&m);
    status = prot.update_data_temperature_double(10.0, 0xffffffffffffffff, &m);
    status = m.serialize(buf);
    buf.print_as_hex();
    util::print_array(sample_message, sample_message_len);
    if (!util::compare_array(sample_message,
                       sample_message_len - sizeof(protocol::crc32_t),
                       reinterpret_cast<uint8_t *>(buf.data()),
                       buf.len() - sizeof(protocol::crc32_t))) {
        printf("buffer does not match sample array\n");
        return false;
    }
    buf.drain(buf.len());
    if (buf.len()) {
        printf("buffer not empty after drain: %zu\n", buf.len());
        return false;
    }
    status = prot.update_data_temperature_double(10.0, 0xffffffffffffffff, &m);
    status = m.serialize(buf);

    buf.print_as_hex();

    // update sequence number
    ++sample_message[8];
    // update crc32
    sample_message[30] = 0x62;
    sample_message[31] = 0xeb;
    sample_message[32] = 0x91;
    sample_message[33] = 0x8a;
    util::print_array(sample_message, sample_message_len);

    return util::compare_array(sample_message,
                         sample_message_len - sizeof(protocol::crc32_t),
                         reinterpret_cast<uint8_t *>(buf.data()),
                         buf.len() - sizeof(protocol::crc32_t));
}

bool test_arg_calc_size()
{
    {
        senack::ArgSingle<double> floatArg{ "f64", protocol::arg_t::f64, 0.0 };
        if (floatArg.calc_size() != sizeof(double)) {
            printf("float size did not match: %zu\n", floatArg.calc_size());
            return false;
        }
    }
    {
        senack::ArgSingle<uint64_t> longIntArg{ "u64", protocol::arg_t::u64, 0 };
        if (longIntArg.calc_size() != sizeof(uint64_t)) {
            printf("long int size did not match: %zu\n", longIntArg.calc_size());
            return false;
        }
    }
    {
        senack::ArgSingle<uint32_t> IntArg{ "u32", protocol::arg_t::u32, 0 };
        if (IntArg.calc_size() != sizeof(uint32_t)) {
            printf("int size did not match: %zu\n", IntArg.calc_size());
            return false;
        }
    }
    return true;
}

bool test_message_payload_calc_size()
{
    senack::Message m;
    m.m.message_id = senack::temperature_f64_data_mid;
    m.args.push_back(new senack::ArgSingle<double>{ "temp_value", protocol::arg_t::f64, 0.0 });
    m.args.push_back(new senack::ArgSingle<util::types::time_us_t>{ "temp_ts", protocol::arg_t::u64, 0 });
    if (m.calc_payload_size() != 16) {
        printf("payload len: %zu\n", m.calc_payload_size());
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    std::pair<std::string, util::test_fn> tests[] = {
        { "test_init_protocol_message", test_init_protocol_message },
        { "test_temp_message", test_temp_message },
        { "test_temp_messageReplace", test_temp_messageReplace },
        { "test_arg_calc_size", test_arg_calc_size },
        { "test_message_payload_calc_size", test_message_payload_calc_size },
    };
    bool all_success = true;
    for (std::pair<std::string, util::test_fn> pair : tests) {
        bool test_success = pair.second();
        printf("test:%s::%s\n", pair.first.c_str(), test_success ? "PASS" : "FAIL");
        all_success = all_success && test_success;
    }
    return all_success ? 0 : -1;
}
